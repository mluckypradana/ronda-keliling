package com.sampulkreativ.rondakeliling.model.core

import java.util.*

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class CoreModel {
    var id: String? = null
    var createdAt: Long? = Calendar.getInstance().time.time
}