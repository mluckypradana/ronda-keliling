package com.sampulkreativ.rondakeliling.model.temp

import com.google.android.gms.maps.model.Marker
import com.sampulkreativ.rondakeliling.model.Checkpoint
import com.sampulkreativ.rondakeliling.model.Member
import com.sampulkreativ.rondakeliling.model.core.CoreModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class CheckpointMarker(member: Checkpoint, marker: Marker) : CoreModel() {
    var checkpoint: Checkpoint? = null
    var marker: Marker? = null

    init {
        this.checkpoint = checkpoint
        this.marker = marker
    }
}