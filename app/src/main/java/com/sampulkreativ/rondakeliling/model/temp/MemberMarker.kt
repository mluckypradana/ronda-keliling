package com.sampulkreativ.rondakeliling.model.temp

import com.google.android.gms.maps.model.Marker
import com.sampulkreativ.rondakeliling.model.Member
import com.sampulkreativ.rondakeliling.model.core.CoreModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class MemberMarker(member: Member, marker: Marker) : CoreModel() {
    var member: Member? = null
    var marker: Marker? = null

    init {
        this.member = member
        this.marker = marker
    }
}