package com.sampulkreativ.rondakeliling.model.core

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class CoreLocationModel: CoreModel() {
    var latitude: Double? = null
    var longitude: Double? = null
}