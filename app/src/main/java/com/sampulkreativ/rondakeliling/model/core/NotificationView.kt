package com.sampulkreativ.rondakeliling.model.core

import com.google.gson.annotations.SerializedName

data class NotificationView(
        @SerializedName("title") val title: String?,
        @SerializedName("body") val body: String?,
        @SerializedName("click_action") val clickAction: String?
)