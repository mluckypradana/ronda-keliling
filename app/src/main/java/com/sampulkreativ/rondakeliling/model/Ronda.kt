package com.sampulkreativ.rondakeliling.model

import com.sampulkreativ.rondakeliling.model.core.CoreModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class Ronda : CoreModel() {
    var placeId: String? = null
    var endAt: Long? = null
}