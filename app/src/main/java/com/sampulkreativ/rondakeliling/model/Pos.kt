package com.sampulkreativ.rondakeliling.model

import com.sampulkreativ.rondakeliling.model.core.CoreLocationModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class Pos : CoreLocationModel() {
    var name: String? = null
    var code: String? = null
}