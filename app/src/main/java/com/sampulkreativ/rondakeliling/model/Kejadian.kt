package com.sampulkreativ.rondakeliling.model

import com.sampulkreativ.rondakeliling.model.core.CoreLocationModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class Kejadian : CoreLocationModel() {
    companion object {
        const val THEFT = 1
        const val FIRE = 2
        const val ACCIDENT = 3
        const val DISASTER = 4
        const val OTHER = 5
    }

    var description: String? = null
    var type: Int? = null
    var memberId: String? = null
    var rondaId: String? = null
    var placeId: String? = null
}