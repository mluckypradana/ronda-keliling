package com.sampulkreativ.rondakeliling.model

import com.sampulkreativ.rondakeliling.model.core.CoreModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class Pengumuman : CoreModel() {
    var description: String? = null
    var title: String? = null
    var placeId: String? = null
}