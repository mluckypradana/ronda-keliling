package com.sampulkreativ.rondakeliling.model

import com.sampulkreativ.rondakeliling.model.core.CoreLocationModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class TitikRonda : CoreLocationModel() {
    var placeId: String? = null
    var code: String? = null
    @Transient
    var checkpoints: MutableList<Checkpoint> = arrayListOf()
}