package com.sampulkreativ.rondakeliling.model

import com.google.android.gms.maps.model.Marker
import com.sampulkreativ.rondakeliling.model.core.CoreLocationModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class Member : CoreLocationModel() {
    var name: String? = null
    var phone: String? = null
    var password: String? = null
    var active: Boolean = true
    var placeId: String? = null
    var placeAdmin: Boolean = false
    @Transient
    var marker: Marker? = null
}