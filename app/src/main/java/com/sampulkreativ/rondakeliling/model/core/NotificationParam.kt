package com.sampulkreativ.rondakeliling.model.core

import com.google.gson.annotations.SerializedName

class NotificationParam(topics: String?, title: String?, message: String?, clickAction: String, type: String, id: String?) {
    @SerializedName("to")
    var to: String? = null
    @SerializedName("data")
    var data: NotificationData? = null
    @SerializedName("notification")
    var notification: NotificationView? = null

    init {
        to = "/topics/$topics"
        data = NotificationData(type, id)
        notification = NotificationView(title, message, clickAction)
    }
}