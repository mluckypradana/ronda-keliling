package com.sampulkreativ.rondakeliling.model.core

import com.google.gson.annotations.SerializedName

data class NotificationResult(
        @SerializedName("message_id") val messageId: Long?
)