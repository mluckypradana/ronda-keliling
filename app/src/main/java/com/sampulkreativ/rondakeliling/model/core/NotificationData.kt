package com.sampulkreativ.rondakeliling.model.core

import com.google.gson.annotations.SerializedName

data class NotificationData(
        @SerializedName("type") val type: String,
        @SerializedName("id") val id: String?
)