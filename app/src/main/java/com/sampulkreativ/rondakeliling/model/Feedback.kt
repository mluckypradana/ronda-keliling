package com.sampulkreativ.rondakeliling.model

import com.sampulkreativ.rondakeliling.model.core.CoreModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class Feedback : CoreModel() {
    var name: String? = null
    var answer1: Int? = 5
    var answer2: Int? = 5
    var answer3: Int? = 4
    var answer4: Int? = 5
    var answer5: Int? = 3
}