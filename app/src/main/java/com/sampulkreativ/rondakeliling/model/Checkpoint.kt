package com.sampulkreativ.rondakeliling.model

import com.sampulkreativ.rondakeliling.model.core.CoreModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class Checkpoint : CoreModel() {
    var rondaId: String? = null
    var pointId: String? = null
    var memberId: String? = null
    @Transient
    var member: Member? = null
}