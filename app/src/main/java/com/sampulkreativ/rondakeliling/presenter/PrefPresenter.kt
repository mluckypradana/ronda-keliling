package com.sampulkreativ.rondakeliling.presenter

import com.orhanobut.hawk.Hawk
import com.rri.customer.config.Prefs
import com.sampulkreativ.rondakeliling.config.Config.CURRENT_PREF_VERSION

/**
 * Created by MuhammadLucky on 12/02/2018.
 */

class PrefPresenter {
    companion object {

        /** Clear all pref related if version or class attributes is upgraded */
        fun checkVersion() {
            val version = Hawk.get<Int?>(Prefs.PREF_VERSION)
            if (version != null && version != CURRENT_PREF_VERSION) {
                deleteAllPrefs()
                Hawk.put(Prefs.PREF_VERSION, CURRENT_PREF_VERSION)
            } else if (version == null)
                Hawk.put(Prefs.PREF_VERSION, CURRENT_PREF_VERSION)
        }

        /** Delete all prefs from hawk */
        private fun deleteAllPrefs() {
            Hawk.deleteAll()
        }

        /** Save temporary data, pass activities */
        fun setData(data: Any) {
            Hawk.put(Prefs.TEMP_DATA, data)
        }

        fun getData(): Any? {
            return Hawk.get(Prefs.TEMP_DATA)
        }


        fun shouldShowIntro(): Boolean {
            return Hawk.get<Boolean>(Prefs.SHOW_INTRO, true)
        }

        fun setShowedIntro() {
            Hawk.put(Prefs.SHOW_INTRO, false)
        }
        fun resetIntro() {
            Hawk.put(Prefs.SHOW_INTRO, true)
        }
    }
}
