package com.sampulkreativ.rondakeliling.presenter

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.provider.Settings
import android.support.v7.app.AlertDialog
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.app.App.Companion.context
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.helper.SingleShotLocationProvider
import com.sampulkreativ.rondakeliling.listener.FetchLocationListener


/**
 * For website information and promotions
 */

class LocationPresenter {
    companion object {

        fun fetchAddress(context: Context, location: Location): Address? {
            try {
                val geocoder = Geocoder(context)
                val results = geocoder.getFromLocation(location.latitude,
                        location.longitude, 1)
                return results[0]
            } catch (ignored: Exception) {
            }

            return null
        }

        fun isWithinRange(locationLatitude: Double?, locationLongitude: Double?, latitude: Double?, longitude: Double?, maxDistanceInMeter: Int): Boolean {
            if (locationLatitude == null || latitude == null) return false
//            val results = FloatArray(1)
//            Location.distanceBetween(locationLatitude, locationLongitude!!, latitude, longitude!!, results)
//            val distanceInMeters = results[0]

            val location = Location("serviceprovider")
            location.latitude = locationLatitude
            location.longitude = locationLongitude!!
            val location2 = Location("serviceprovider")
            location2.latitude = latitude
            location2.longitude = longitude!!
            val distanceInMeters = location.distanceTo(location2)
//            Common.showToast("Distance: " + distanceInMeters.toString())
            return distanceInMeters < maxDistanceInMeter
        }

        fun setDefaultCamera(map: GoogleMap?) {
            val defaultLocation = LatLng(-6.873586, 107.561796)
            map?.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLocation, 17F))
        }
    }

    private var googleApiClient: GoogleApiClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationManager: LocationManager? = null

    fun fetchLocation(context: Context, activity: Activity, callback: FetchLocationListener?) {
        locationManager = App.context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showActivateGpsDialog(context, activity, callback)
            callback?.onFetchLocationCanceled()
        } else
            requestLocation(activity, callback)
    }


    private fun requestLocation(activity: Activity, callback: FetchLocationListener?) {
        Dexter.withActivity(activity)
                .withPermissions(arrayListOf(Manifest.permission.ACCESS_FINE_LOCATION))
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        Common.showToast(R.string.message_permission_location)
                        callback?.onFetchLocationFailure()
                    }

                    @SuppressLint("MissingPermission")
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted())
                            connectGoogleApi(callback)
                        else
                            callback?.onFetchLocationFailure()
                    }
                }).check()
    }


    @SuppressLint("MissingPermission")
    private fun connectGoogleApi(callback: FetchLocationListener?) {
        SingleShotLocationProvider.requestSingleUpdate(context) { location ->
            run {
                val result = Location("")
                result.latitude = location.latitude.toDouble()
                result.longitude = location.longitude.toDouble()
                callback?.onFetchLocationSuccess(result)
            }
        }
    }

    internal fun showActivateGpsDialog(context: Context, activity: Activity, callback: FetchLocationListener?) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context, R.style.AppTheme_Dialog_Alert)
        builder.setMessage(App.Companion.context.getString(R.string.message_enable_permission_location))
                .setCancelable(true)
                .setPositiveButton("Aktifkan") { _, _ ->
                    activity.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
                .setOnCancelListener { callback?.onFetchLocationFailure() }
        val alert = builder.create()
        alert.show()
    }

    /**
     * Show maps direction with intent view to maps apps.
     * @param context The context
     * @param slatitude The Start latitude string
     * @param slongitude The Start longitude string
     * @param elatitude The end latitude string
     * @param elongitude The end longitude string
     */
    fun showMapsDirection(context: Context, slatitude: Double?, slongitude: Double?, elatitude: Double?, elongitude: Double?) {
        val gmmIntentUri = Uri.parse("http://maps.google.com/maps?tf=d&saddr=" + slatitude + "," + slongitude + "" +
                "&daddr=" + elatitude + "," + elongitude)
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.`package` = "com.google.android.apps.maps"
        if (mapIntent.resolveActivity(context.packageManager) != null)
            context.startActivity(mapIntent)
        else
            Common.showMessageDialog(context = context, message = context.getString(R.string.error_maps_direction))
    }

    fun showMapsDirection(context: Context, startLocation: Location, endLocation: Location) {
        showMapsDirection(context, startLocation.latitude, startLocation.longitude, endLocation.latitude, endLocation.longitude)
    }
}

