package com.sampulkreativ.rondakeliling.presenter

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.messaging.FirebaseMessaging
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.kopnus.kdigi.presenter.callback.RondaStateCallback
import com.orhanobut.hawk.Hawk
import com.rri.customer.config.Db
import com.rri.customer.config.Prefs
import com.rri.customer.helper.LocationHelper
import com.rri.customer.helper.ValidationHelper
import com.rri.customer.presenter.core.CorePresenter
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.model.Member
import com.sampulkreativ.rondakeliling.model.Pos
import com.sampulkreativ.rondakeliling.model.TitikRonda


/**
 * Created by MuhammadLucky on 12/02/2018.
 */

class MemberPresenter : CorePresenter() {
    companion object {
        private var presenter: MemberPresenter = MemberPresenter()

        fun getMember(): Member? {
            return presenter.getMember()
        }

        fun getMemberPlace(): Pos? {
            return presenter.getMemberPlace()
        }

        fun updateMemberLocation(location: Location?) {
            var member: Member? = getMember() ?: return

            if (location != null) {
                //TODO Only update when distance is significant

                member?.latitude = location.latitude
                member?.longitude = location.longitude
            } else {
                member?.latitude = null
                member?.longitude = null
            }
            presenter.saveMember(member)
            val memberId = member?.id
            memberId?.let { member?.let {
                it1 -> presenter.db.collection(Db.MEMBERS).document(it).set(it1) }
            }
        }

        fun updateMemberLocation(location: LatLng) {
            var loc = LocationHelper.createByDouble(location.latitude, location.longitude)
            if (loc != null)
                updateMemberLocation(loc)
        }

        fun isAdmin(): Boolean {
            var member = presenter.getMember()
            return member?.placeAdmin ?: false
        }


    }

    fun login(phone: String?, password: String?, callback: PostDataCallback?) {
        //Validate
        val resId = when {
            phone.isNullOrEmpty() -> R.string.error_empty_phone
            ValidationHelper.lessThan(R.integer.min_phone, phone) -> R.string.error_min_phone
            password.isNullOrEmpty() -> R.string.error_empty_password
            ValidationHelper.hasWhitespace(password) -> R.string.error_min_password
            else -> 0
        }
        if (resId != 0 && Config.ENABLE_VALIDATION) {
            callback?.onValidation(App.context.getString(resId))
            return
        }

        val encryptedPassword = encryption(password)
        db.collection(Db.MEMBERS)
                .whereEqualTo("phone", phone)
                .whereEqualTo("password", encryptedPassword)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val data = getSingleData(task, Member::class.java)
                        if (data != null) {
                            saveMemberAfterLogin(data)
                            callback?.onSuccess(data)
                        } else
                            callback?.onFailure(str(R.string.error_failed_login))

                    } else handleError(task.exception, callback)
                }
    }


    fun saveMember(data: Member?) {
        Hawk.put(Prefs.USER, data)
    }

    fun saveMember(data: Member?, pos: Pos?) {
        Hawk.put(Prefs.USER, data)
        if (pos != null) {
            savePlace(pos)
            FirebaseMessaging.getInstance().subscribeToTopic(pos.id)
        }
    }

    fun saveMemberAfterLogin(data: Member?) {
        Hawk.put(Prefs.USER, data)
        PosPresenter().getPlace(data?.placeId!!, object : FetchDataCallback {
            override fun onFailure(message: String?) {
            }

            override fun onSuccess(data: Any?) {
                var pos = data as Pos?
                if (pos != null) {
                    savePlace(pos)
                    FirebaseMessaging.getInstance().subscribeToTopic(pos.id)
                }
            }
        })
    }

    private fun savePlace(place: Pos?) {
        Hawk.put(Prefs.PLACE, place)
    }

    fun getMember(): Member? {
        return Hawk.get(Prefs.USER)
    }

    fun getMemberPlace(): Pos? {
        return Hawk.get(Prefs.PLACE)
    }

    fun logout() {
        val pos = MemberPresenter.getMemberPlace()
        pos?.let {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(it.id)
        }
        updateMemberLocation(null)
        Hawk.delete(Prefs.ORDER_PRODUCTS)
        Hawk.delete(Prefs.USER)
    }

    fun registerAsRt(name: String, phone: String, password: String, callback: PostDataCallback?) {
        //Validate
        var resId = when {
            name.isEmpty() -> R.string.error_empty_admin_name
            ValidationHelper.lessThan(R.integer.min_name, name) -> R.string.error_min_name
            phone.isEmpty() -> R.string.error_empty_phone
            ValidationHelper.lessThan(R.integer.min_phone, phone) -> R.string.error_min_phone
            password.isEmpty() -> R.string.error_empty_password
            ValidationHelper.lessThan(R.integer.min_password, password) -> R.string.error_min_password
            else -> 0
        }
        if (resId != 0 && Config.ENABLE_VALIDATION) {
            callback?.onValidation(App.context.getString(resId))
            return
        }

        var member = Member()
        member.name = name
        member.phone = phone
        member.password = encryption(password)
        member.placeAdmin = true
        PrefPresenter.setData(member)
        callback?.onSuccess()
    }

    fun registerAsRt2(placeName: String, location: Location?, callback: PostDataCallback?) {
        //Validate
        var resId = when {
            placeName.isEmpty() -> R.string.error_empty_place_name
            ValidationHelper.lessThan(R.integer.min_name, placeName) -> R.string.error_min_place_name
            location == null -> R.string.error_empty_location
            else -> 0
        }
        if (resId != 0 && Config.ENABLE_VALIDATION) {
            callback?.onValidation(App.context.getString(resId))
            return
        }

        var user = PrefPresenter.getData() as Member
        var place = PosPresenter().createPlace(placeName, location?.latitude, location?.longitude)
        user.placeId = place.id

        // Add a new document with a generated ID
        var trans = db.collection(Db.MEMBERS).document()
        user.id = trans.id
        trans.set(user)
                .addOnSuccessListener { documentReference ->
                    saveMember(user, place)
                    callback?.onSuccess(str(R.string.success_register))
                }
                .addOnFailureListener { e ->
                    handleError(e, callback)
                }
    }

    fun registerAsMember(name: String, phone: String, password: String, posCode: String, callback: PostDataCallback?) {
        //Validate
        var resId = when {
            name.isEmpty() -> R.string.error_empty_name
            ValidationHelper.lessThan(R.integer.min_name, name) -> R.string.error_min_name
            phone.isEmpty() -> R.string.error_empty_phone
            ValidationHelper.lessThan(R.integer.min_phone, phone) -> R.string.error_min_phone
            password.isEmpty() -> R.string.error_empty_password
            ValidationHelper.lessThan(R.integer.min_password, password) -> R.string.error_min_password
            posCode.isEmpty() -> R.string.error_empty_pos_code
            ValidationHelper.lessThan(R.integer.max_pos_code, posCode) -> R.string.error_min_pos_code
            else -> 0
        }
        if (resId != 0 && Config.ENABLE_VALIDATION) {
            callback?.onValidation(App.context.getString(resId))
            return
        }

        //Check if pos code is exists
        PosPresenter().getPosByCode(posCode, object : FetchDataCallback {
            override fun onFailure(message: String?) {
                callback?.onFailure(message)
            }

            override fun onSuccess(data: Any?) {

                //Proceeed to next step
                var user = Member()
                user.name = name
                user.phone = phone
                user.password = encryption(password)
                user.placeId = (data as Pos).id

                // Add a new document with a generated ID
                var trans = db.collection(Db.MEMBERS).document()
                user.id = trans.id
                trans.set(user)
                        .addOnSuccessListener { documentReference ->
//                            saveMember(user)
//                            savePlace(data)
                            callback?.onSuccess(str(R.string.success_add_member))
                        }
                        .addOnFailureListener { e ->
                            handleError(e, callback)
                        }
            }
        })


    }

    fun getPasswordFromPos(): String {
        var user = MemberPresenter.getMemberPlace() ?: return ""
        return user.id!!.toLowerCase().substring(0, 6)
    }


    fun getMembers(realtime: Boolean, callback: FetchDataCallback?) {
        var call = db.collection(Db.MEMBERS)
                .whereEqualTo("placeId", getMemberPlace()?.id)
//                .whereEqualTo("placeAdmin", false)

        if (realtime)
            call.addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
                if (e != null) {
                    handleError(e, callback)
                    return@EventListener
                }
                val list = getMultipleData(value, Member::class.java)
                callback?.onSuccess(list.asReversed())
            })
        else
            call.get().addOnCompleteListener { task ->
                if (task.isSuccessful)
                    callback?.onSuccess(getMultipleData(task.result, Member::class.java))
                else
                    handleError(task.exception, callback)
            }

    }

    /** Realtime */
    fun getMembers(callback: FetchDataCallback?) {
        getMembers(true, callback)
    }

    fun checkRondaState(callback: RondaStateCallback?) {
        getMembers(false, object : FetchDataCallback {
            override fun onFailure(message: String?) {
            }

            override fun onSuccess(data: Any?) {
                var list = data as MutableList<Member>
                var memberTotal = list.size

                if (memberTotal == 0)
                    callback?.onMemberEmpty()
                else
                    PointPresenter().getPoints(object : FetchDataCallback {
                        override fun onFailure(message: String?) {
                        }

                        override fun onSuccess(data: Any?) {
                            var list = data as MutableList<TitikRonda>
                            var pointTotal = list.size
                            if (pointTotal == 0)
                                callback?.onPointEmpty()
                            else
                                callback?.onRondaAvailable()
                        }
                    })
            }
        })
    }
}
