package com.sampulkreativ.rondakeliling.presenter

import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.QuerySnapshot
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.api.callback.CoreCallback
import com.rri.customer.config.ClickAction
import com.rri.customer.config.Db
import com.rri.customer.presenter.core.CorePresenter
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.model.Pengumuman
import com.sampulkreativ.rondakeliling.model.core.NotificationParam
import com.sampulkreativ.rondakeliling.model.core.NotificationResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by MuhammadLucky on 12/02/2018.
 */

class AnnouncePresenter : CorePresenter() {
    fun announce(title: String, description: String, callback: PostDataCallback?) {
        //Validate
        var resId = when {
            title.isEmpty() -> R.string.error_empty_title
            description.isEmpty() -> R.string.error_empty_announcement_description
            else -> 0
        }
        if (resId != 0 && Config.ENABLE_VALIDATION) {
            callback?.onValidation(App.context.getString(resId))
            return
        }

        //Proceeed to next step
        var data = Pengumuman()
        data.title = title
        data.description = description
        data.placeId = MemberPresenter.getMemberPlace()?.id

        // Add a new document with a generated ID
        var trans = db.collection(Db.ANNOUNCEMENTS).document()
        data.id = trans.id
        trans.set(data)
                .addOnSuccessListener { documentReference ->
                    sendNotification(data, callback)
//                    callback?.onSuccess(str(R.string.success_announce))
                }
                .addOnFailureListener { e ->
                    handleError(e, callback)
                }
    }

    private fun sendNotification(data: Pengumuman, callback: CoreCallback?) {
        val param = NotificationParam(data.placeId, data.title, data.description,
                ClickAction.ANNOUNCEMENTS, Db.ANNOUNCEMENTS, data.id)
        val call = App.api.sendNotification(param)
        call.enqueue(object : Callback<NotificationResult> {
            override fun onFailure(call: Call<NotificationResult>, t: Throwable) {
                callback?.onFailure(str(R.string.error_generic))
            }

            override fun onResponse(call: Call<NotificationResult>, response: Response<NotificationResult>) {
                if (response.isSuccessful)
                    callback?.onSuccess(str(R.string.success_announce))
                else
                    callback?.onFailure(str(R.string.error_generic))
            }
        })
    }

    fun getAnnouncements(callback: FetchDataCallback?) {
        var call = db.collection(Db.ANNOUNCEMENTS)
                .whereEqualTo("placeId", MemberPresenter.getMemberPlace()?.id)

        call.addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
            if (e != null) {
                handleError(e, callback)
                return@EventListener
            }
            val list = getMultipleData(value, Pengumuman::class.java)
            list.sortByDescending { it.createdAt }
            callback?.onSuccess(list)
        })
    }

    fun getAnnouncement(id: String, callback: FetchDataCallback?) {
        db.collection(Db.ANNOUNCEMENTS).document(id)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val data = task.result.toObject(Pengumuman::class.java)
                        if (data != null)
                            callback?.onSuccess(data)
                        else
                            callback?.onFailure(str(R.string.error_generic))

                    } else handleError(task.exception, callback)
                }
    }
}
