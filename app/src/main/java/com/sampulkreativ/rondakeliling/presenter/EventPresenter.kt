package com.sampulkreativ.rondakeliling.presenter

import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.QuerySnapshot
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.config.ClickAction
import com.rri.customer.config.Db
import com.rri.customer.presenter.core.CorePresenter
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.model.Kejadian
import com.sampulkreativ.rondakeliling.model.Member
import com.sampulkreativ.rondakeliling.model.Ronda
import com.sampulkreativ.rondakeliling.model.core.NotificationParam
import com.sampulkreativ.rondakeliling.model.core.NotificationResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by MuhammadLucky on 12/02/2018.
 */

class EventPresenter : CorePresenter() {
    fun report(type: Int, ronda: Ronda?, otherDescription: String, callback: PostDataCallback?) {
        //Validate
        var resId = when {
            type == Kejadian.OTHER && otherDescription.isEmpty() -> R.string.error_empty_event_description
            else -> 0
        }
        if (resId != 0 && Config.ENABLE_VALIDATION) {
            callback?.onValidation(App.context.getString(resId))
            return
        }

        //Proceeed to next step
        var data = Kejadian()
        var member = MemberPresenter.getMember()!!
        data.type = type
        data.description = if (type == Kejadian.OTHER) otherDescription else null
        data.memberId = member.id
        data.rondaId = ronda?.id
        data.latitude = member.latitude
        data.longitude = member.longitude
        data.placeId = MemberPresenter.getMemberPlace()?.id

        // Add a new document with a generated ID
        var trans = db.collection(Db.EVENTS).document()
        data.id = trans.id
        trans.set(data)
                .addOnSuccessListener { documentReference ->
                    sendNotification(data, member)
                    callback?.onSuccess(str(R.string.success_report_event))
                }
                .addOnFailureListener { e ->
                    handleError(e, callback)
                }
    }

    private fun sendNotification(data: Kejadian, member: Member) {
        val type = when (data.type) {
            Kejadian.OTHER -> data.description
            Kejadian.DISASTER -> App.context.getString(R.string.action_disaster)
            Kejadian.ACCIDENT -> App.context.getString(R.string.action_accident)
            Kejadian.FIRE -> App.context.getString(R.string.action_fire)
            Kejadian.THEFT -> App.context.getString(R.string.action_theft)
            else -> ""
        }

        var description = member.name + " melaporkan kejadian di lokasi."

        val param = NotificationParam(data.placeId, type, description,
                ClickAction.EVENTS, Db.EVENTS, data.id)
        val call = App.api.sendNotification(param)
        call.enqueue(object : Callback<NotificationResult> {
            override fun onFailure(call: Call<NotificationResult>, t: Throwable) {
            }

            override fun onResponse(call: Call<NotificationResult>, response: Response<NotificationResult>) {
            }
        })
    }

    fun getEvents(callback: FetchDataCallback?) {
        var call = db.collection(Db.EVENTS)
                .whereEqualTo("placeId", MemberPresenter.getMemberPlace()?.id)

        call.addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
            if (e != null) {
                handleError(e, callback)
                return@EventListener
            }
            val list = getMultipleData(value, Kejadian::class.java)
            list.sortByDescending { it.createdAt }
            callback?.onSuccess(list)
        })
    }

    fun getEvent(id: String?, callback: FetchDataCallback?) {
        if(id==null) {
            callback?.onFailure(str(R.string.error_generic))
            return
        }
        db.collection(Db.EVENTS).document(id)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val data = task.result.toObject(Kejadian::class.java)
                        if (data != null)
                            callback?.onSuccess(data)
                        else
                            callback?.onFailure(str(R.string.error_generic))

                    } else handleError(task.exception, callback)
                }
    }

}
