package com.sampulkreativ.rondakeliling.presenter

import com.rri.customer.config.Db
import com.rri.customer.presenter.core.CorePresenter
import com.sampulkreativ.rondakeliling.model.Feedback


/**
 * Created by MuhammadLucky on 12/02/2018.
 */

class FeedbackPresenter : CorePresenter() {
    fun putFeedback() {
        val list = arrayListOf<Feedback>()
        list.add(Feedback())
        list.add(Feedback())
        list.add(Feedback())
        list.add(Feedback())
        list.add(Feedback())
        list.add(Feedback())
        list.add(Feedback())
        list.add(Feedback())

        for(data in list) {
            // Add a new document with a generated ID
            var trans = db.collection(Db.FEEDBACKS).document()
            data.id = trans.id
            trans.set(data)
                    .addOnSuccessListener { documentReference ->

                    }
                    .addOnFailureListener { e ->
                        handleError(e, null)
                    }
        }
    }

}
