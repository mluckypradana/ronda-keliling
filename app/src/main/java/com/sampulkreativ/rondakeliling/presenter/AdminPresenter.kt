package com.sampulkreativ.rondakeliling.presenter

import android.location.Location
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.QuerySnapshot
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.orhanobut.hawk.Hawk
import com.rri.customer.config.Db
import com.rri.customer.config.Prefs
import com.rri.customer.presenter.core.CorePresenter
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.model.Member


/**
 * Created by MuhammadLucky on 12/02/2018.
 */

class AdminPresenter : CorePresenter() {
    fun login(username: String, password: String, callback: PostDataCallback?) {
        //Validate
        val resId = when {
            username.isEmpty() -> R.string.error_empty_username
            password.isEmpty() -> R.string.error_empty_password
            else -> 0
        }
        if (resId != 0 && Config.ENABLE_VALIDATION) {
            callback?.onValidation(App.context.getString(resId))
            return
        }

        if (username == str(R.string.admin_username) && password == str(R.string.admin_password)) {
            Hawk.put(Prefs.LOGIN_AS_ADMIN, true)
            callback?.onSuccess()
        } else
            callback?.onFailure(str(R.string.error_generic))
    }

    fun logout() {
        Hawk.put(Prefs.LOGIN_AS_ADMIN, false)
    }

    fun isAdminLoggedIn(): Boolean {
        return Hawk.get(Prefs.LOGIN_AS_ADMIN, false)
    }


    fun getMembers(callback: FetchDataCallback?) {
        var call = db.collection(Db.MEMBERS)
//                .whereEqualTo("placeId", getMemberPlace()?.id)
//                .whereEqualTo("placeAdmin", false)

        call.addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
            if (e != null) {
                handleError(e, callback)
                return@EventListener
            }
            val list = getMultipleData(value, Member::class.java)
            callback?.onSuccess(list.asReversed())
        })

    }

    fun setMemberLocation(member: Member?, location: Location?, callback: PostDataCallback?) {
        //Validate
        val resId = when {
            member == null -> R.string.error_empty_member
            location == null -> R.string.error_empty_location
            else -> 0
        }
        if (resId != 0 && Config.ENABLE_VALIDATION) {
            callback?.onValidation(App.context.getString(resId))
            return
        }

        member?.latitude = location?.latitude
        member?.longitude = location?.longitude
        db.collection(Db.MEMBERS).document(member?.id!!).set(member)
        callback?.onSuccess()
    }
}
