package com.sampulkreativ.rondakeliling.presenter

import android.location.Location
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.QuerySnapshot
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.config.Db
import com.rri.customer.presenter.core.CorePresenter
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.model.Checkpoint
import com.sampulkreativ.rondakeliling.model.Member
import com.sampulkreativ.rondakeliling.model.Ronda
import com.sampulkreativ.rondakeliling.model.TitikRonda


/**
 * Created by MuhammadLucky on 12/02/2018.
 */

class PointPresenter : CorePresenter() {

    fun getPoints(callback: FetchDataCallback?) {
        getPoints(true, callback)
    }

    /** Realtime */
    fun getPoints(realtime: Boolean, callback: FetchDataCallback?) {
        var call = db.collection(Db.POINTS)
                .whereEqualTo("placeId", MemberPresenter.getMemberPlace()?.id)

        if (realtime)
            call.addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
                if (e != null) {
                    handleError(e, callback)
                    return@EventListener
                }
                callback?.onSuccess(getMultipleData(value, TitikRonda::class.java))
            })
        else
            call.get().addOnCompleteListener { task ->
                if (task.isSuccessful)
                    callback?.onSuccess(getMultipleData(task.result, TitikRonda::class.java))
                else
                    handleError(task.exception, callback)
            }
    }

    fun registerPoint(text: String?, location: Location?, callback: PostDataCallback?) {
        //Validate
        var resId = when {
            location == null -> R.string.error_empty_point
            else -> 0
        }
        if (resId != 0 && Config.ENABLE_VALIDATION) {
            callback?.onValidation(App.context.getString(resId))
            return
        }

        var data = TitikRonda()
        data.placeId = MemberPresenter.getMemberPlace()?.id
        data.latitude = location?.latitude
        data.longitude = location?.longitude
        data.code = text

        // Add a new document with a generated ID
        var trans = db.collection(Db.POINTS).document()
        data.id = trans.id
        trans.set(data)
                .addOnSuccessListener { documentReference ->
                    callback?.onSuccess(data)
                }
                .addOnFailureListener { e ->
                    handleError(e, callback)
                }
    }

    fun removePoint(point: TitikRonda?, callback: PostDataCallback?) {
        db.collection(Db.POINTS).document(point!!.id!!).delete()
        callback?.onSuccess()
    }

    fun updatePoint(point: TitikRonda, code: String?, callback: PostDataCallback?) {
        point.code = code
        db.collection(Db.POINTS).document(point.id!!).set(point)
        callback?.onSuccess()
    }

    fun getPointCheckpoints(ronda: Ronda?, points: MutableList<TitikRonda>, callback: FetchDataCallback?) {
        if (ronda == null) {
            callback?.onSuccess()
            return
        }

        val call = db.collection(Db.CHECKPOINTS)
                .whereEqualTo("rondaId", ronda.id)
//                .whereGreaterThan("createdAt", ronda.createdAt!!)
        call.addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
            if (e != null) {
                handleError(e, callback)
                return@EventListener
            }
            var list = getMultipleData(value, Checkpoint::class.java)
            list.sortBy { it.createdAt }
            populateCheckpointsToPoint(list, points)
            callback?.onSuccess()
        })
    }

    fun getCheckpoints(ronda: Ronda?, callback: FetchDataCallback?) {
        if (ronda == null) {
            callback?.onSuccess()
            return
        }

        MemberPresenter().getMembers(false, object : FetchDataCallback {
            override fun onFailure(message: String?) {
                callback?.onFailure(message)
            }

            override fun onSuccess(response: Any?) {
                val members = response as MutableList<Member>

                val call = db.collection(Db.CHECKPOINTS)
                        .whereEqualTo("rondaId", ronda.id)
                call.get().addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        var list = getMultipleData(task.result, Checkpoint::class.java)
                        list.sortBy { it.createdAt }
                        populateMemberToCheckpoint(list, members)
                        callback?.onSuccess(list)
                    } else
                        handleError(task.exception, callback)
                }

                call.addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
                    if (e != null) {
                        handleError(e, callback)
                        return@EventListener
                    }

                })
            }
        })
    }

    private fun populateMemberToCheckpoint(list: MutableList<Checkpoint>, members: MutableList<Member>) {
        for (data in list) {
            val member = members.find { it.id == data.memberId }
            if (member != null)
                data.member = member
        }
    }

    private fun populateCheckpointsToPoint(checkpoints: MutableList<Checkpoint>, points: MutableList<TitikRonda>) {
        for (point in points) {
            var checkpointList = checkpoints.filter { it.pointId == point.id }
            point.checkpoints.clear()
            point.checkpoints.addAll(checkpointList)
        }
    }
}
