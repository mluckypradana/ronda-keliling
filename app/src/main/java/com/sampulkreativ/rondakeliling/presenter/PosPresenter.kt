package com.sampulkreativ.rondakeliling.presenter

import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.rri.customer.config.Db
import com.rri.customer.presenter.core.CorePresenter
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.model.Pos


/**
 * Created by MuhammadLucky on 12/02/2018.
 */

class PosPresenter : CorePresenter() {

    fun createPlace(placeName: String?, latitude: Double?, longitude: Double?): Pos {
        var data = Pos()
        data.name = placeName
        data.latitude = latitude
        data.longitude = longitude

        // Add a new document with a generated ID
        var trans = db.collection(Db.POS).document()
        data.id = trans.id
        data.code = trans.id.toLowerCase().substring(0, 6)
        trans.set(data)
                .addOnSuccessListener { documentReference ->
                    //                    saveMember(user)
//                    callback?.onSuccess(str(R.string.success_register))
                }
                .addOnFailureListener { e ->
                    //                    handleError(e, callback)
                }
        return data
    }

    fun getPlace(id: String, callback: FetchDataCallback?) {
        db.collection(Db.POS).document(id)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val data = task.result.toObject(Pos::class.java)
                        if (data != null)
                            callback?.onSuccess(data)
                        else
                            callback?.onFailure(str(R.string.error_generic))

                    } else handleError(task.exception, callback)
                }
    }

    fun getPlaces(callback: FetchDataCallback?) {
        db.collection(Db.POS).get().addOnCompleteListener { task ->
            if (task.isSuccessful)
                callback?.onSuccess(getMultipleData(task.result, Pos::class.java))
            else
                handleError(task.exception, callback)
        }
    }

    fun getPosByCode(posCode: String, callback: FetchDataCallback?) {
        db.collection(Db.POS) .whereEqualTo("code", posCode)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val data = getSingleData(task, Pos::class.java)
                        if (data != null) {
                            callback?.onSuccess(data)
                        } else
                            callback?.onFailure(str(R.string.error_invalid_pos_code))

                    } else handleError(task.exception, callback)
                }
    }
}
