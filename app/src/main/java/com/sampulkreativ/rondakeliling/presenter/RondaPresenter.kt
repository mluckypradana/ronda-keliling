package com.sampulkreativ.rondakeliling.presenter

import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.QuerySnapshot
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.api.callback.RondaEvent
import com.rri.customer.config.ClickAction
import com.rri.customer.config.Db
import com.rri.customer.presenter.core.CorePresenter
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.model.Checkpoint
import com.sampulkreativ.rondakeliling.model.Ronda
import com.sampulkreativ.rondakeliling.model.TitikRonda
import com.sampulkreativ.rondakeliling.model.core.NotificationParam
import com.sampulkreativ.rondakeliling.model.core.NotificationResult
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter.Companion.getMemberPlace
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


/**
 * Created by MuhammadLucky on 12/02/2018.
 */

class RondaPresenter : CorePresenter() {
    fun startRonda(latitude: Double?, longitude: Double?, callback: PostDataCallback?) {

        val place = MemberPresenter.getMemberPlace()

        //Validate
        var resId = when {
            latitude == null -> R.string.error_empty_location_ronda
//            !LocationPresenter.isWithinRange(place?.latitude, place?.longitude, latitude, longitude,
//                    App.context.resources.getInteger(R.integer.max_verify_distance)) -> R.string.error_location_start_ronda
            else -> 0
        }
        if (resId != 0 && Config.ENABLE_VALIDATION) {
            callback?.onValidation(App.context.getString(resId))
            return
        }

        var data = Ronda()
//        data.memberId = MemberPresenter.getMember()!!.id
        data.placeId = place!!.id

        // Add a new document with a generated ID
        var trans = db.collection(Db.RONDA).document()
        data.id = trans.id
        trans.set(data)
                .addOnSuccessListener { documentReference ->
                    sendStartNotification(data)
                    callback?.onSuccess(data)

                }
                .addOnFailureListener { e ->
                    handleError(e, callback)
                }
    }

    private fun sendStartNotification(data: Ronda) {
        val param = NotificationParam(data.placeId, str(R.string.title_ronda_started), str(R.string.message_ronda_started),
                ClickAction.RONDA, Db.RONDA, data.id)
        val call = App.api.sendNotification(param)
        call.enqueue(object : Callback<NotificationResult> {
            override fun onFailure(call: Call<NotificationResult>, t: Throwable) {
            }

            override fun onResponse(call: Call<NotificationResult>, response: Response<NotificationResult>) {
            }
        })
    }


    fun isRondaStarted(callback: FetchDataCallback?) {
        db.collection(Db.RONDA)
                .whereEqualTo("endAt", null)
                .whereEqualTo("placeId", MemberPresenter.getMemberPlace()?.id)
                .addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
                    if (e != null) {
                        handleError(e, callback)
                        return@EventListener
                    }
                    val list = getMultipleData(value, Ronda::class.java)
                    list.sortByDescending { it.createdAt }
                    val ronda = if (list.isNotEmpty()) list[0] else null
//                    callback?.onSuccess(ronda)
                    EventBus.getDefault().post(RondaEvent(ronda))
                })

//                .get()
//                .addOnCompleteListener { task ->
//                    if (task.isSuccessful) {
//                        val data = getSingleData(task, Ronda::class.java)
//                        if (data != null) {
//                            callback?.onSuccess(data)
//                        } else
//                            callback?.onFailure(str(R.string.error_generic))
//
//                    } else handleError(task.exception, callback)
//                }
    }

    fun stopRonda(ronda: Ronda?, callback: PostDataCallback?) {
        var trans = db.collection(Db.RONDA).document(ronda!!.id!!)
        ronda.endAt = Calendar.getInstance().time.time
        trans.set(ronda)
                .addOnSuccessListener { documentReference ->
                    callback?.onSuccess(ronda)
                }
                .addOnFailureListener { e ->
                    handleError(e, callback)
                }
    }

    fun addCheckpoint(text: String?, ronda: Ronda?, points: MutableList<TitikRonda>, callback: PostDataCallback?) {
        var member = MemberPresenter.getMember()
        var point = points.find { it.code == text }
        if (point != null) {
            if (LocationPresenter.isWithinRange(point?.latitude, point?.longitude, member?.latitude, member?.longitude,
                            App.context.resources.getInteger(R.integer.max_verify_distance))) {
                var data = Checkpoint()
                data.rondaId = ronda?.id
                data.pointId = point.id
                data.memberId = member?.id

                // Add a new document with a generated ID
                var trans = db.collection(Db.CHECKPOINTS).document()
                data.id = trans.id
                trans.set(data)
                        .addOnSuccessListener { documentReference ->
                            callback?.onSuccess(data)
                        }
                        .addOnFailureListener { e ->
                            handleError(e, callback)
                        }
            } else
                callback?.onValidation(str(R.string.error_failed_scan))
        } else
            callback?.onValidation(str(R.string.error_failed_scan_code))

    }

    fun getRondaList(realtime: Boolean, callback: FetchDataCallback?) {
        var call = db.collection(Db.RONDA)
                .whereEqualTo("placeId", getMemberPlace()?.id)

        if (realtime)
            call.addSnapshotListener(EventListener<QuerySnapshot> { value, e ->
                if (e != null) {
                    handleError(e, callback)
                    return@EventListener
                }
                val list = getMultipleData(value, Ronda::class.java)
                list.sortByDescending { it.createdAt }
                callback?.onSuccess(list)
            })
        else
            call.get().addOnCompleteListener { task ->
                if (task.isSuccessful)
                    callback?.onSuccess(getMultipleData(task.result, Ronda::class.java))
                else
                    handleError(task.exception, callback)
            }

    }
}
