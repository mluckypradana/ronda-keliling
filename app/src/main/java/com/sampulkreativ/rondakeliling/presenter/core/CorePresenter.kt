package com.rri.customer.presenter.core

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.QuerySnapshot
import com.rri.customer.api.callback.CoreCallback
import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.BuildConfig
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.app.App
import com.scottyab.aescrypt.AESCrypt
import java.security.GeneralSecurityException


open class CorePresenter {
    var db = FirebaseFirestore.getInstance()

    init {
        setOffline(false)
    }

    fun setOffline(enabled: Boolean) {
        val settings = FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(enabled)
                .build()
        db.firestoreSettings = settings
    }
//    var api: Api.APIInterface = App.api
//
//    /** If response is invalid return false & callback.onFailure */
//    internal fun isInvalid(response: Response<*>?, callback: CoreCallback?): Boolean {
//        //If response is valid
//        if (response!!.isSuccessful && response.body() != null) {
//            //Validate response body
////            try {
////                response.body() as CoreResp
////            } catch (ignored: Exception) {
////                callback?.onFailure(
////                        if (BuildConfig.FLAVOR != "production") ignored.localizedMessage
////                        else App.context.getString(R.string.error_generic))
////                return true
////            }
//            return false
//        }
//
//        //If response code is invalid
//        else {
//            var errorResponse: ErrorResp? = null
//            //Get error body
//            try {
//                val bytes = response.errorBody()!!.bytes()
//                errorResponse = Gson().fromJson(String(bytes, Charset.forName("UTF-8")), ErrorResp::class.java)
//            } catch (ignored: Exception) {
//            }
//
//            //Need auto logout
//            if (response.code() == 403)
//                callback?.onFailure(App.context.getString(R.string.message_session_ended))
//
//            //Another common error
//            else
//                callback?.onFailure(
//                        when {
//                            errorResponse != null -> errorResponse.message
//                            BuildConfig.FLAVOR == "production" -> App.context.getString(R.string.error_generic)
//                            response.code() == 400 -> App.context.getString(R.string.error_conn_400)
//                            response.code() == 401 -> App.context.getString(R.string.error_conn_401)
//                            response.code() == 404 -> App.context.getString(R.string.error_conn_404)
//                            response.code() == 408 -> App.context.getString(R.string.error_conn_408)
//                            response.code() == 501 -> App.context.getString(R.string.error_conn_501)
//                            response.code() == 502 -> App.context.getString(R.string.error_conn_502)
//                            response.code() == 503 -> App.context.getString(R.string.error_conn_503)
//                            response.code() == 504 -> App.context.getString(R.string.error_conn_504)
//                            else -> App.context.getString(R.string.error_generic)
//                        }
//                )
//            return true
//        }
//    }
//
//    /** If api call failed, call callback.onFailure */

    fun <T> getSingleData(task: Task<QuerySnapshot>, java: Class<T>): T? {
        if (task.result.size() == 0) return null
        var data: T? = null
        for (document in task.result) {
            data = document.toObject(java)
            break
        }
        return data
    }

    internal fun <T> getMultipleData(value: QuerySnapshot?, java: Class<T>): MutableList<T> {
        val list: MutableList<T> = arrayListOf()
        for (document in value!!) {
            val data = document.toObject(java)
            list.add(data)
        }
        return list
    }

    internal fun handleError(t: Throwable?, callback: CoreCallback?) {
        callback?.onFailure(
                if (!BuildConfig.DEBUG) App.context.getString(R.string.error_generic)
                else t?.localizedMessage
        )
    }

    internal fun str(resId: Int): String {
        return App.context.getString(resId)
    }

    fun encryption(strNormalText: String?): String {
//        if (strNormalText == null) return ""
//
//        val seedValue = Config.ENCRYPT_KEY
//        var normalTextEnc = ""
//        try {
//            normalTextEnc = AESHelper.encrypt(seedValue, strNormalText)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }

//        return normalTextEnc
//        return encryption.encryptOrNull(strNormalText)
        try {
            return AESCrypt.encrypt(Config.ENCRYPT_KEY, strNormalText)
        } catch (e: GeneralSecurityException) {
            //handle error
        }
        return ""
    }

    fun decryption(strEncryptedText: String?): String {
//        if (strEncryptedText == null) return ""
//        val seedValue = Config.ENCRYPT_KEY
//        var strDecryptedText = ""
//        try {
//            strDecryptedText = AESHelper.decrypt(seedValue, strEncryptedText)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//
//        return strDecryptedText
//        return encryption.decryptOrNull(strEncryptedText)
        try {
            return AESCrypt.decrypt(Config.ENCRYPT_KEY, strEncryptedText);
        } catch (e: GeneralSecurityException) {
            //handle error - could be due to incorrect password or tampered encryptedMsg
        }
        return ""
    }

}
