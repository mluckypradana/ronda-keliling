package com.rri.customer.helper

import java.text.NumberFormat
import java.util.*

/**
 * Utilities to convert string / double value to currency format
 * Created by MuhammadLucky on 20/02/2018.
 */

class CurrencyHelper {
    companion object {
        fun toCurrency(value: Double): String {
//            val localeID = Locale("in", "ID")
            var tempValue = round(value)
//            val formatRupiah = NumberFormat.getCurrencyInstance(localeID)
//            return formatRupiah.format(value)

            //Second option
//            val formatter = NumberFormat.getInstance(Locale.US) as DecimalFormat
//            formatter.applyPattern("#.###.###.###")
//            val formattedString = formatter.format(value)

            //Last option
//            val df = NumberFormat.getCurrencyInstance()
//            df.maximumFractionDigits = 0
//            val dfs = DecimalFormatSymbols()
//            dfs.currencySymbol = ""//""Rp. "
//            dfs.groupingSeparator = '.'
//            dfs.monetaryDecimalSeparator = '.'
//            dfs.decimalSeparator = ','
//            (df as DecimalFormat).decimalFormatSymbols = dfs
//            return df.format(value)

            //Last option
            var str = NumberFormat.getNumberInstance(Locale.US).format(tempValue)
            if (str.contains(".00") && str.length > 3)
                str = str.substring(0, str.length - 3)
            str = str.replace(".", "#")
            str = str.replace(",", ".")
            str = str.replace("#", ",")
            return str
        }

        fun toPoint(point: Long?): String? {
            var str = NumberFormat.getNumberInstance(Locale.US).format(point)
            if (str.contains(".00") && str.length > 3)
                str = str.substring(0, str.length - 3)
            return str
        }

        fun toCurrency(text: String?): String {
            if (text == null || text.isEmpty())
                return "0"

            return try {
                val value = java.lang.Double.valueOf(text)!!
                toCurrency(round(value))
            } catch (e: Exception) {
                "0"
            }
        }

        fun toCurrency(value: Long?): String {
            if (value == null || value == 0L)
                return "0"
            return toCurrency(value.toDouble())
        }

        fun getValue(currency: String?): Long {
            if (ValidationHelper.isEmpty(currency)) return 0

            var value = currency
            if (value == null) value = "0"
            value = value.replace(".", "")
            value = value.replace(",", ".")
            return value.toLong()
        }

        fun round(d: Double): Long {
//            val dAbs = Math.abs(d)
            val i = d.toLong()
//            val result = dAbs - i.toDouble()
//            return if (result < 0.5) {
//                if (d < 0) -i else i
//            } else {
//                if (d < 0) -(i + 1) else i + 1
//            }
            return i
        }

        fun round2Decimal(d: Double): String {
            return String.format(Locale.ENGLISH, "%.2f", d)
        }
    }
}
