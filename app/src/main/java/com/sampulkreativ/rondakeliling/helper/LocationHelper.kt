package com.rri.customer.helper

import android.location.Location

/**
 * *
 * Created by MuhammadLucky on 02/03/2018.
 */

object LocationHelper {
    fun createByDouble(latitude: Double?, longitude: Double?): Location? {
        if (latitude == null || longitude == null) return null
        var location = Location("serviceprovider")
        location.latitude = latitude
        location.longitude = longitude
        return location
    }
}
