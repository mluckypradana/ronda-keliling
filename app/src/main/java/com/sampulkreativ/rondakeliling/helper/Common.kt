package com.sampulkreativ.rondakeliling.helper

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Toast
import com.sampulkreativ.rondakeliling.BuildConfig
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.fragment.ConfirmDialogFragment
import com.sampulkreativ.rondakeliling.listener.OnClickListener

/**
 * Created by Alvin Rusli on 1/24/2016.
 *
 * A class that handles basic universal methods.
 */
object Common {

    /** The loading progress dialog object */
    var progressDialog: ProgressDialog? = null

    /** Show custom confirmation dialog with specific header icon*/
    fun showConfirmDialog(supportFragmentManager: FragmentManager, headerIconResId: Int, message: String?, listener: OnClickListener?) {
        val newFragment = ConfirmDialogFragment()
        newFragment.setHeaderIcon(headerIconResId)
        newFragment.setMessage(message)
        newFragment.setListener(listener)
        newFragment.show(supportFragmentManager, "confirm_dialog")
    }

    /** Show custom confirmation dialog*/
    fun showConfirmDialog(supportFragmentManager: FragmentManager, message: String?, listener: OnClickListener?) {
        showConfirmDialog(supportFragmentManager, R.mipmap.ic_launcher, message, listener)
    }

    /**
     * Shows a loading progress dialog.
     * @param context the context
     * @param stringRes the dialog message string resource id
     * @param onBackPressListener the back button press listener when loading is shown
     */
    fun showProgressDialog(context: Context, stringRes: Int = -1, cancelable: Boolean? = false) {
        dismissProgressDialog()

        progressDialog = ProgressDialog(context, R.style.MyProgressDialog)
        progressDialog!!.setMessage(context.getString(stringRes))
        if (cancelable != null)
            progressDialog!!.setCancelable(cancelable)
//        progressDialog!!.backPressListener = onBackPressListener
        if (context is Activity && !context.isFinishing) progressDialog!!.show()
    }

    /** Hides the currently shown loading progress dialog */
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
            progressDialog = null
        }
    }


    /**
     * Sets the progress dialog progress indeterminate state.
     * @param isIndeterminate Determines if progress dialog is indeterminate
     */
    fun setProgressDialogIndeterminate(isIndeterminate: Boolean) {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.setIndeterminate(isIndeterminate)
        }
    }

    /**
     * Sets the progress dialog message.
     * @param message The dialog message string
     */
    fun setProgressDialogText(message: String) {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.setMessage(message)
        }
    }

    /**
     * Display a simple [Toast].
     * @param stringRes The message string resource id
     */
    fun showToast(stringRes: Int) {
        showToast(App.context.getString(stringRes))
    }

    /**
     * Display a simple [Toast].
     * @param message The message string
     */
    fun showToast(message: String?) {
        message?.let { Toast.makeText(App.context, message, Toast.LENGTH_SHORT).show() }
    }

    /**
     * Display a simple [AlertDialog] with a simple OK button.
     * If the dismiss listener is specified, the dialog becomes uncancellable
     * @param context The context
     * @param title The title string
     * @param message The message string
     * @param dismissListener The dismiss listener
     */
    fun showMessageDialog(
            context: Context = App.context,
            title: String? = null,
            message: String?,
            dismissListener: DialogInterface.OnDismissListener? = null) {
        val builder = AlertDialog.Builder(context, 0)// R.style.AppTheme_Dialog_Alert)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton(android.R.string.ok) { dialog, _ -> dialog.dismiss() }

        val dialog = builder.create()
        if (dismissListener != null) {
            dialog.setOnDismissListener(dismissListener)
//            dialog.setCancelable(false)
//            dialog.setCanceledOnTouchOutside(false)
        }
        dialog.show()
    }

    /**
     * Prints an exception's stack trace.
     * Stack traces printed via this method will only show up on debug builds.
     * @param throwable the throwable
     */
    fun printStackTrace(throwable: Throwable) {
        if (BuildConfig.DEBUG) throwable.printStackTrace()
    }

    /**
     * Prints a [Log] message.
     * Log messages printed via this method will only show up on debug builds.
     * @param type The specified log type, may be [Log.DEBUG], [Log.INFO], and other log types
     * @param tag The log tag to print
     * @param message The log message to print
     */
    fun log(type: Int = Log.DEBUG, tag: String? = "BaseProject", message: String?) {
        if (BuildConfig.DEBUG) {
            var logMessage = message
            if (logMessage.isNullOrEmpty()) logMessage = "Message is null, what exactly do you want to print?"
            Log.println(type, tag, logMessage)
        }
    }


    /** Prevent null string to "" */
    fun preventNull(text: String?): String {
        return if (text == null) "" else text
    }
}
