package com.sampulkreativ.rondakeliling.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sampulkreativ.rondakeliling.R;
import com.sampulkreativ.rondakeliling.app.App;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public final class FontHelper {

    public static void setDefaultFont(Context context,
                                      String staticTypefaceFieldName, String fontAssetName) {
        final Typeface regular = Typeface.createFromAsset(context.getAssets(),
                fontAssetName);
        replaceFont(staticTypefaceFieldName, regular);
    }

    protected static void replaceFont(String staticTypefaceFieldName,
                                      final Typeface newTypeface) {
        //Regular override
        try {
            final Field staticField = Typeface.class
                    .getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Also override font for lollipop or above version
        if (Build.VERSION.SDK_INT >= 21) {
            Map<String, Typeface> newMap = new HashMap<>();
            newMap.put("sans-serif", newTypeface);
            try {
                final Field staticField = Typeface.class
                        .getDeclaredField("sSystemFontMap");
                staticField.setAccessible(true);
                staticField.set(null, newMap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Customize font to specific font face.
     *
     * @param textView TextView component
     */
    public static void customizeFont(Context context, TextView textView, String fontFace) {
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), fontFace);
        textView.setTypeface(typeFace);
        textView.invalidate();
    }

    public static void setAllTextView(Context context, ViewGroup parent, String fontFace) {
        for (int i = parent.getChildCount() - 1; i >= 0; i--) {
            final View child = parent.getChildAt(i);
            if (child instanceof ViewGroup) {
                setAllTextView(context, (ViewGroup) child, fontFace);
            } else if (child instanceof TextView) {
                customizeFont(context, (TextView) child, fontFace);
            }
        }
    }

    public static void applyFontForToolbarTitle(Toolbar toolbar) {
        if (toolbar == null) return;
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                Typeface titleFont = Typeface.
                        createFromAsset(App.Companion.getContext().getAssets(), App.Companion.getContext().getString(R.string.font_light));
                if (tv.getText().equals(toolbar.getTitle())) {
                    tv.setTypeface(titleFont);
                    break;
                }
            }
        }
    }
}