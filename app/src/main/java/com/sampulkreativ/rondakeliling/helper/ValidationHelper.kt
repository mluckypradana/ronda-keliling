package com.rri.customer.helper

import android.util.Patterns
import android.view.View
import android.widget.TextView
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.app.App
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by Alvin on 10/10/16.
 *
 * A validation helper class.
 */
object ValidationHelper {

    /**
     * Check if a specified [String] is a valid [JSONObject] or [JSONArray].
     * @param message the [String] to check
     * @return true if JSON is valid
     */
    fun isInvalidJson(message: String): Boolean {
        try {
            JSONObject(message)
        } catch (ex: JSONException) {
            // Check if JSONArray is valid as well…
            try {
                JSONArray(message)
            } catch (ex1: JSONException) {
                return true
            }
        }

        return false
    }

    fun hasWhitespace(text: String?): Boolean {
        if (text == null)
            return true
        return text.contains(" ")
    }

    /**
     * Checks if a [String] is empty.
     * Also checks for whitespaces, so if the text is "   " it will be treated as an empty String.
     * Only checks for visible [TextView].
     * @param textView the [TextView] containing the [String] to check
     * @return true if empty, returns a false when [TextView] isn't visible / null
     */
    fun isEmpty(textView: TextView?): Boolean {
        if (textView?.visibility == View.VISIBLE) return isEmpty(textView.text.toString().trim())
        else return false
    }

    /**
     * Checks if a [String] is empty.
     * Also checks for whitespaces, so if the text is "   " it will be treated as an empty String.
     * @param text the string to check
     * @return true if empty
     */
    fun isEmpty(text: String?): Boolean {
        if (text.isNullOrEmpty()) return true

        if (text!!.replace(" ".toRegex(), "").isEmpty()) return true
        return false
    }

    /** Checks if spinner / value is empty */
    fun isEmpty(value: Int?): Boolean {
        if (value != null) return value < 0
        return true
    }

    /** Checks if value is empty */
    fun isEmpty(value: Long?): Boolean {
        if (value != null) return value <= 0
        return true
    }

    /**
     * Checks if group of string [String] is empty.
     * Also checks for whitespaces, so if the text is "   " it will be treated as an empty String.
     * @param text the string to check
     * @return true if empty
     */
    fun isEmpty(vararg texts: String?): Boolean {
        for (text: String? in texts) {
            if (text.isNullOrEmpty()) return true
            if (text!!.replace(" ".toRegex(), "").isEmpty()) return true
        }
        return false
    }

    /** Checks if group of values is empty */
    fun isEmpty(vararg values: Int?): Boolean {
        return values.any { isEmpty(it) }
    }

    /**
     * Determines if a name input is more than 2 char.
     * @param text the string to check
     */
    fun isInvalidName(text: String?): Boolean {
        if (text.isNullOrEmpty()) return true
        return text?.length!! < App.context.resources.getInteger(R.integer.min_name)
    }

    /**
     * Determines if a password input is valid.
     * Only checks for visible [TextView].
     * @param textView the [TextView] containing the [String] to check
     * @return false if password is invalid, returns a true when [TextView] isn't visible / null
     */
    fun isInvalidPassword(textView: TextView?): Boolean {
        if (textView?.visibility == View.VISIBLE) return isInvalidPassword(textView.text.toString().trim())
        else return false
    }

    /**
     * Determines if a password input is valid.
     * @param text the string to check
     * @return false if password is invalid
     */
    fun isInvalidPassword(text: String?): Boolean {
        if (text.isNullOrEmpty()) return true
        return text?.length!! < App.context.resources.getInteger(R.integer.min_password)
    }

    /**
     * Validate if phone starts with 0
     * And min length is 13
     * Return message resid if phone sis invalid
     */
    fun validatePhone(text: String?): Int {
        if (text.isNullOrEmpty()) return 0
        var resId = 0
        if (!text!!.startsWith("0"))
            resId = R.string.error_invalid_phone_prefix
        if (text.length < App.context.resources.getInteger(R.integer.min_phone))
            resId = R.string.error_min_phone
        return resId
    }

    /**
     * Determines if an email input is valid.
     * Only checks for visible [TextView].
     * @param textView the [TextView] containing the [String] to check
     * @return false if email is invalid, returns a true when [TextView] isn't visible / null
     */
    fun isInvalidEmail(textView: TextView?): Boolean {
        if (textView?.visibility == View.VISIBLE) return isInvalidEmail(textView.text.toString().trim())
        else return false
    }

    /**
     * Determines if an email input is valid.
     * @param text the string to check
     * @return true if it's a valid email
     */
    fun isInvalidEmail(text: String?): Boolean {
        if (text.isNullOrEmpty()) return true
        return !Patterns.EMAIL_ADDRESS.matcher(text).matches()
    }

    /**
     * Remove all whitespaces from a string
     */
    fun removeWhitespaces(text: String?): String? {
        return text?.replace(" ", "")
    }

    fun lessThan(minLengthResId: Int, text: String?): Boolean {
        if (text.isNullOrEmpty()) return true
        return text!!.length < App.context.resources.getInteger(minLengthResId)
    }

}
