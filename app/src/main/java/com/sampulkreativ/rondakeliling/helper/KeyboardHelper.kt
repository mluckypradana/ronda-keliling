package com.rri.customer.helper

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager

/**
 * *
 * Created by MuhammadLucky on 02/03/2018.
 */

class KeyboardHelper {
    companion object {
        fun hideKeyboard(context: Activity?) {
            if (context == null) return

            // Check if no view has focus:
            val view = context.currentFocus
            if (view != null) {
                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
    }
}
