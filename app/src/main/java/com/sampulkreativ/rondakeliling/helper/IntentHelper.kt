package com.sampulkreativ.rondakeliling.helper

import android.content.Context
import android.content.Intent

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
class IntentHelper {
    companion object {

        fun launch(context: Context, java: Class<*>) {
            val intent = Intent(context, java)
            context.startActivity(intent)
        }
    }
}