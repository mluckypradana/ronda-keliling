package com.rri.customer.helper

import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.helper.Common
import java.text.ParseException

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Alvin Rusli on 10/7/2016.
 *
 * A helper class for date/time format.
 */
object DateHelper {

    /**
     * Convert time and date to millis.
     * @param dateTime The string to parse
     * @return time in millis
     */
    @Throws(ParseException::class)
    fun convertToMillis(dateTime: String, dateFormat: String): Long {
        val formatter = SimpleDateFormat(dateFormat, Locale.US)
        return formatter.parse(dateTime).time
    }

    /**
     * Obtain a [Calendar] object from a String.
     * @return the [Calendar] object
     */
    fun getCalendarFromMillis(millis: Long): Calendar {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = millis
        return calendar
    }

    /**
     * Obtain a [Calendar] object from a String.
     * @return the [Calendar] object
     */
    fun getCalendarFromDate(dateTime: String, dateFormat: String): Calendar? {
        try {
            val formatter = SimpleDateFormat(dateFormat, Locale.US)
            val date = formatter.parse(dateTime)
            val calendar = Calendar.getInstance()
            calendar.time = date
            return calendar
        } catch (e: ParseException) {
            Common.printStackTrace(e)
            return null
        }
    }


    fun parseDate(time: String, inputPattern: String, outputPattern: String): String? {
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)

        var date: Date? = null
        var str: String? = null

        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return str
    }

    fun parseToSimpleDate(time: String): String? {
        val inputPattern = "dd/MM/yyyy HH:mm:ss"
        val outputPattern = "dd-MMM"//-yyyy h:mm a"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)

        var date: Date?
        var str: String? = null

        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return str
    }

    fun parseDate(time: String?, outputPattern: String): String? {
        if (time == null) return time

        val inputPattern = "dd/MM/yyyy HH:mm:ss"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.US)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.US)

        val date: Date?
        var str: String? = null

        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            return time
        }

        return str
    }

    fun format(milis: Long?, format: String = Config.DATE_FORMAT): String {
        val format1 = SimpleDateFormat(format, Locale.getDefault())
        return if(milis!=null) format1.format(milis) else ""
    }
}
