package com.rri.customer.api.callback

class QrCodeEvent(text: String) {
    var text: String? = null

    init {
        this.text = text
    }
}
