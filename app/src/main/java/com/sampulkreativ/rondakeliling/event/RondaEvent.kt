package com.rri.customer.api.callback

import com.sampulkreativ.rondakeliling.model.Ronda

class RondaEvent(ronda: Ronda?) {
    var ronda: Ronda? = null

    init {
        this.ronda = ronda
    }
}
