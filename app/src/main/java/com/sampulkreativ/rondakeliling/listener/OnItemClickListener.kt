package com.rri.customer.listener

/**
 * Created by Alvin Rusli on 06/09/2017.
 * General on item click listener for recyclerview adapter
 */
interface OnItemClickListener {
    fun onItemClicked(position: Int)
}
