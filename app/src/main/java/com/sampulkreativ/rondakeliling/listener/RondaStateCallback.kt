package com.kopnus.kdigi.presenter.callback

/**
 * Created by MuhammadLucky on 05/02/2018.
 */
interface RondaStateCallback  {
    fun onMemberEmpty()
    fun onPointEmpty()
    fun onRondaAvailable()
}
