package com.kopnus.kdigi.presenter.callback

import com.rri.customer.api.callback.CoreCallback

/**
 * Created by MuhammadLucky on 05/02/2018.
 */
interface FetchDataCallback: CoreCallback
