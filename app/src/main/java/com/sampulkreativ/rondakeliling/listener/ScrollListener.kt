package com.rri.customer.listener

/**
 * Created by MuhammadLucky on 24/08/2016.
 */
interface ScrollListener {
    fun onScrolledTop()

    fun onScrolledBottom()
}
