package com.sampulkreativ.rondakeliling.listener

import android.location.Location

/**
 * Custom listener for fetch location in LocationPresenter
 */
interface FetchLocationListener {
    fun onFetchLocationSuccess(location: Location?)
    fun onFetchLocationFailure()
    fun onFetchLocationCanceled()
}
