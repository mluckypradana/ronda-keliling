package com.sampulkreativ.rondakeliling.api

import com.sampulkreativ.rondakeliling.model.core.NotificationParam
import com.sampulkreativ.rondakeliling.model.core.NotificationResult
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface ApiService {

    @Headers("Authorization: key=AAAADUg6O2Q:APA91bGBYLAPP6opWPy_TXF0bBwOoheVz6K_6TpWswFXf1Y2UM5NiF1qkAlEByeQugdNnL5U9s6AOgzPK95ojjj-oWtd0z6scINqqdxD44Z0Pj3oKVHtFmcJ-maiG-NZDgXUiN3bgeT9")
    @POST("send")
    fun sendNotification(
                 @Body body: NotificationParam)
            : Call<NotificationResult>
}