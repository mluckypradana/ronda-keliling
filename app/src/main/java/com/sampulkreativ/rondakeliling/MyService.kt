package com.sampulkreativ.rondakeliling

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.location.Location
import android.media.RingtoneManager
import android.os.Binder
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.sampulkreativ.rondakeliling.activity.MainAct
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.fragment.MainFragment
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter


class MyService : Service(), LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    override fun onLocationChanged(p0: Location?) {
        //If app locked
        if (!MainFragment.ACTIVE)
            MemberPresenter.updateMemberLocation(p0)
    }

    fun isActivityRunning(ctx: Context): Boolean {
        val activityManager = ctx.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val tasks = activityManager.getRunningTasks(Integer.MAX_VALUE)

        for (task in tasks) {
            if (ctx.packageName.equals(task.baseActivity.packageName, ignoreCase = true))
                return true
        }

        return false
    }

    override fun onConnected(p0: Bundle?) {
        startLocationUpdates()
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    private val TAG = "LocationActivity"
    private val INTERVAL = (1000 * 20).toLong()
    private val FASTEST_INTERVAL = (1000 * 10).toLong()
    lateinit var mLocationRequest: LocationRequest
    var mCurrentLocation: Location? = null
    var mLastUpdateTime: String? = null
    private lateinit var mGoogleApiClient: GoogleApiClient

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        createLocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
        mGoogleApiClient.connect()
//
//        if (mGoogleApiClient.isConnected) {
//        startLocationUpdates()
//            Log.d(TAG, "Location update resumed .....................")
//        }

        //smsHandler.sendEmptyMessageDelayed(DISPLAY_DATA, 1000);
        return Service.START_STICKY
    }


    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    inner class MyBinder : Binder() {
        val service: MyService
            get() = this@MyService
    }

    private fun addNotification(location: Location) {
        val intent = Intent(this, MainAct::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.notification_channel_default_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val smallIcon = R.mipmap.ic_launcher
        val largeIcon = R.mipmap.ic_launcher
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(smallIcon)
                .setLargeIcon(BitmapFactory.decodeResource(applicationContext.resources, largeIcon))
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Lokasi: " + location.latitude + ", " + location.longitude)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = INTERVAL
        mLocationRequest.fastestInterval = FASTEST_INTERVAL
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
//        mLocationRequest.smallestDisplacement = Config.UPDATE_DISTANCE
    }

    private fun isGooglePlayServicesAvailable(): Boolean {
        val status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this)
        return ConnectionResult.SUCCESS == status
    }

    private lateinit var mFusedLocationClient: FusedLocationProviderClient

    private lateinit var locationCallback: LocationCallback

    @SuppressLint("MissingPermission")
    protected fun startLocationUpdates() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (isAppRunning(App.context, BuildConfig.APPLICATION_ID)) return

                val locationList = locationResult?.locations
                var size = locationList?.size ?: 0
                if (size > 0)
                    locationList?.get(0)?.let {
                        MemberPresenter.updateMemberLocation(it)
//                        addNotification(it)
                    }
            }
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        val pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this)

        Log.d(TAG, "Location update started ..............: ")
    }

    fun isAppRunning(context: Context, packageName: String): Boolean {
        val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val procInfos = activityManager.runningAppProcesses
        if (procInfos != null) {
            for (processInfo in procInfos) {
                if (processInfo.processName == packageName) {
                    return true
                }
            }
        }
        return false
    }

    protected fun stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(locationCallback)
        Log.d(TAG, "Location update stopped .......................")
    }

    override fun onDestroy() {
        mGoogleApiClient.disconnect()
        super.onDestroy()

    }
}