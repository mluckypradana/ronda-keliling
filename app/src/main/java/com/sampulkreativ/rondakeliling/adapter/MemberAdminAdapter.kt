package com.sampulkreativ.rondakeliling.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rri.customer.adapter.core.CoreAdapter
import com.rri.customer.listener.OnItemClickListener
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.model.Member
import kotlinx.android.synthetic.main.item_member.view.*

/**
 * Created by MuhammadLucky on 24/11/2017.
 */

class MemberAdminAdapter(list: MutableList< Member>, listener: OnItemClickListener) : CoreAdapter<Member>() {
    val layout = R.layout.item_member_admin

    init {
        this.list = list
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind()
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val data = list[adapterPosition]
            itemView.tv_title.text = data.name
            itemView.tv_info.text = data.phone
            itemView.iv_call.visibility = View.GONE
            itemView.setOnClickListener { listener?.onItemClicked(adapterPosition) }
        }

    }
}