package com.rri.customer.adapter.core

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.fondesa.recyclerviewdivider.RecyclerViewDivider
import com.rri.customer.listener.OnItemClickListener
import com.rri.customer.listener.ScrollListener
import com.sampulkreativ.rondakeliling.app.App
import java.util.*

/**
 * Created by MuhammadLucky on 24/11/2017.
 */

open class CoreAdapter<T> : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    internal var context: Context? = null
    var list: MutableList<T> = ArrayList()
    var listener: OnItemClickListener? = null
    private var scrollListener: ScrollListener? = null
    private var currentTotal: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    /**
     * Add scroll listener with custom interface ScrollListener
     *
     * @param listener As fragmnent, implemented with listener
     * @param dataList As list of object
     * @return Scroll listener for recyclerview
     */
    fun getScrollListener(listener: ScrollListener?, dataList: List<T>): RecyclerView.OnScrollListener {
        this.list = dataList as MutableList<T>
        scrollListener = listener
        return object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val mLayoutManager = recyclerView!!.layoutManager as LinearLayoutManager
                //Scroll down
                if (dy > 0) {
                    //If position equals to last (two) position
                    val currentLastItemPosition = mLayoutManager.findLastVisibleItemPosition()
                    if (list.size != currentTotal &&
                            list.size > 1 &&
                            currentLastItemPosition >= list.size - 1 && listener != null) {
                        scrollListener?.onScrolledBottom()
                        currentTotal = list.size
                    }
                } else {
                    //If position equals to first position
                    val currentFirstItemPosition = mLayoutManager.findFirstCompletelyVisibleItemPosition()
                    if (currentFirstItemPosition == 0 && scrollListener != null)
                        scrollListener!!.onScrolledTop()
                }
            }
        }
    }

    fun addSeparator(recyclerView: RecyclerView, separatorResId: Int) {
        RecyclerViewDivider.Builder(App.context)
                .size(App.context.resources.getDimensionPixelSize(separatorResId))
                .asSpace().hideLastDivider().build()
                .addTo(recyclerView)
    }
}
