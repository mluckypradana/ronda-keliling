package com.sampulkreativ.rondakeliling.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rri.customer.adapter.core.CoreAdapter
import com.rri.customer.helper.DateHelper
import com.rri.customer.listener.OnItemClickListener
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.model.Pengumuman
import kotlinx.android.synthetic.main.item_member.view.*

/**
 * Created by MuhammadLucky on 24/11/2017.
 */

class PengumumanAdapter(list: MutableList<Pengumuman>, listener: OnItemClickListener) : CoreAdapter<Pengumuman>() {
    val layout = R.layout.item_pengumuman

    init {
        this.list = list
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind()
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val data = list[adapterPosition]
            itemView.setOnClickListener { listener?.onItemClicked(adapterPosition) }
            itemView.tv_title.text = data.title
            itemView.tv_info.text = DateHelper.format(milis = data.createdAt)
            itemView.tv_description.text = data.description
        }

    }
}