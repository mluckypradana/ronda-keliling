package com.sampulkreativ.rondakeliling.app

import android.annotation.SuppressLint
import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import android.support.v7.app.AppCompatDelegate
import com.google.firebase.messaging.FirebaseMessaging
import com.orhanobut.hawk.Hawk
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.api.ApiService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by MuhammadLucky on 14/05/2018.
 */
class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        context = this
//        api = Api.getAPIInterface()

        //Init hawk preference
        Hawk.init(context).build()
        initTopicFirebase()
        initRetrofit()
    }

    private fun initRetrofit() {
        val retrofit = Retrofit.Builder()
                .baseUrl("https://fcm.googleapis.com/fcm/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        api = retrofit.create<ApiService>(ApiService::class.java)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val notificationChannel = NotificationChannel(
                    getString(R.string.notification_channel_default_id),
                    getString(R.string.notification_channel_default_name),
                    NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    private fun initTopicFirebase() {
        FirebaseMessaging.getInstance().subscribeToTopic("android")
    }

    companion object {
        lateinit var api: ApiService

        /**
         * The application [Context] made static.
         * Do **NOT** use this as the context for a view,
         * this is mostly used to simplify calling of resources
         * (esp. String resources) outside activities.
         */
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
//        lateinit var api: Api.APIInterface
//            private set


        // This flag should be set to true to enable VectorDrawable support for API < 21
        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }

        //Activity list to finish
        var activities: MutableList<Activity> = arrayListOf()
    }
}