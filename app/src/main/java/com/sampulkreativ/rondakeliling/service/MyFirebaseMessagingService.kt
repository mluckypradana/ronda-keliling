package com.sampulkreativ.rondakeliling.service

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.support.v4.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.rri.customer.config.ClickAction
import com.sampulkreativ.rondakeliling.R


/**
 * Created by MuhammadLucky on 9/25/2018.
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String?) {
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token)
    }

    private fun sendRegistrationToServer(token: String?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        val title = remoteMessage!!.notification!!.title
        val message = remoteMessage.notification!!.body
        val clickAction = remoteMessage.notification!!.clickAction
        val intent = Intent(clickAction)
        intent.putExtra("id", remoteMessage.data["id"])
        intent.putExtra("type", remoteMessage.data["type"])
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val builder = NotificationCompat.Builder(this)
        builder.setContentTitle(title)
        builder.setContentText(message)
        builder.setSmallIcon(R.mipmap.ic_launcher)
        builder.setBadgeIconType(R.mipmap.ic_launcher)
        builder.setAutoCancel(true)
        builder.setContentIntent(pendingIntent)

        if (clickAction == ClickAction.EVENTS)
            try {
                val notification = Uri.parse("android.resource://" + packageName + "/" + R.raw.event)
                val r = RingtoneManager.getRingtone(applicationContext, notification)
                r.play()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        else {
            //Sound
            val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            builder.setSound(soundUri)
        }

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, builder.build())
    }
}