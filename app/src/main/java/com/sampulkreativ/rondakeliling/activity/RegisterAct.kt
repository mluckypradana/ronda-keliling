package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.os.Bundle
import com.google.android.gms.maps.GoogleMap
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.activity.core.CoreActivity
import com.rri.customer.function.body
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import kotlinx.android.synthetic.main.act_register.*


class RegisterAct : CoreActivity() {
    override val viewRes: Int = R.layout.act_register
    val presenter: MemberPresenter = MemberPresenter()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b_invite.setOnClickListener { invite() }
        b_proceed.setOnClickListener { proceed() }
    }


    private fun proceed() {
        showLoading(this)
        presenter.registerAsRt(et_name.body(), et_phone.body(), et_password.body(), object : PostDataCallback {
            override fun onValidation(message: String) {
                Common.showToast(message)
                hideLoading()
            }

            override fun onFailure(message: String?) {
                Common.showToast(message)
                hideLoading()
            }

            override fun onSuccess(data: Any?) {
                launchActivity(Register2Act::class.java)
                hideLoading()
            }
        })
    }


    private fun invite() {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        shareIntent.type = "text/plain"
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.message_invite))
        startActivity(shareIntent)
    }
}