package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.os.Bundle
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.activity.core.CoreActivity
import com.rri.customer.function.body
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import kotlinx.android.synthetic.main.act_register_member.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class AddMemberAct : CoreActivity() {
    override val viewRes: Int = R.layout.act_add_member
    val presenter: MemberPresenter = MemberPresenter()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar(toolbar)
        b_proceed.setOnClickListener { proceed() }
        initView()
    }

    private fun initView() {
        et_code.setText(MemberPresenter().getPasswordFromPos())
    }


    private fun proceed() {
        showLoading(this)
        presenter.registerAsMember(et_name.body(), et_phone.body(), et_password.body(), et_code.body(), object : PostDataCallback {
            override fun onValidation(message: String) {
                Common.showToast(message)
                hideLoading()
            }

            override fun onFailure(message: String?) {
                Common.showToast(message)
                hideLoading()
            }

            override fun onSuccess(data: Any?) {
                Common.showToast(data as String)
                hideLoading()
                finish()
            }
        })
    }
}