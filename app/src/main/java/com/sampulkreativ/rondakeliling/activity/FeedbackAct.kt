package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.os.Bundle
import com.rri.customer.activity.core.CoreActivity
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import kotlinx.android.synthetic.main.act_feedback.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class FeedbackAct : CoreActivity() {
    override val viewRes: Int = R.layout.act_feedback
    val presenter: MemberPresenter = MemberPresenter()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar(toolbar)

        b_proceed.setOnClickListener { proceed() }
    }

    private fun proceed() {

    }
}
