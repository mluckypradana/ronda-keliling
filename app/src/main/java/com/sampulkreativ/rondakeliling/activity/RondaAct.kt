package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.rri.customer.activity.core.CoreActivity
import com.rri.customer.function.body
import com.rri.customer.listener.OnItemClickListener
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.adapter.CheckpointAdapter
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.app.App.Companion.context
import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.helper.ResourceHelper
import com.sampulkreativ.rondakeliling.model.Checkpoint
import com.sampulkreativ.rondakeliling.model.Pos
import com.sampulkreativ.rondakeliling.model.Ronda
import com.sampulkreativ.rondakeliling.model.TitikRonda
import com.sampulkreativ.rondakeliling.model.temp.CheckpointMarker
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import com.sampulkreativ.rondakeliling.presenter.PointPresenter
import com.sampulkreativ.rondakeliling.presenter.PosPresenter
import com.sampulkreativ.rondakeliling.presenter.PrefPresenter
import kotlinx.android.synthetic.main.act_ronda.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class RondaAct : CoreActivity(), OnMapReadyCallback {
    override val viewRes: Int = R.layout.act_ronda
    val presenter: MemberPresenter = MemberPresenter()
    val pointPresenter = PointPresenter()
    val placePresenter = PosPresenter()
    private val pointList: MutableList<TitikRonda> = arrayListOf()
    private val list: MutableList<Checkpoint> = arrayListOf()
    private val originalList: MutableList<Checkpoint> = arrayListOf()
    private val checkpointMarkers: MutableList<CheckpointMarker> = arrayListOf()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar(toolbar)

        et_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                searchName(et_name.body())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        initList()
        initMap()
        initData()
    }


    /** Search account based on search text */
    private fun searchName(text: String) {
        adapter.list.clear()
        if (text.isEmpty())
            adapter.list.addAll(originalList)
        else {
            var filteredList = originalList.filter { it.member != null && it.member!!.name!!.contains(text, true) }
            adapter.list.addAll(filteredList)
        }
        adapter.notifyDataSetChanged()
    }


    private lateinit var adapter: CheckpointAdapter

    private fun initList() {
        adapter = CheckpointAdapter(list, object : OnItemClickListener {
            override fun onItemClicked(position: Int) {
                showPoint(position)
            }
        })
        rv_main.layoutManager = LinearLayoutManager(App.context, LinearLayoutManager.VERTICAL, false)
        rv_main.adapter = adapter
        adapter.addSeparator(rv_main, R.dimen.card_space)
    }

    private fun showPoint(position: Int) {
        val data = list[position]
        val point = pointList.find { it.id == data.pointId }
        if (point != null) {
            val location = LatLng(point.latitude!!, point.longitude!!)
            map.animateCamera(CameraUpdateFactory.newLatLng(location))
        }
    }

    private lateinit var ronda: Ronda

    private fun initData() {
        ronda = PrefPresenter.getData() as Ronda
    }

    private fun initCheckpoints() {
        pointPresenter.getCheckpoints(ronda, object : FetchDataCallback {
            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(data: Any?) {
                var listData = data as MutableList<Checkpoint>
                list.clear()
                list.addAll(listData)
                originalList.clear()
                originalList.addAll(listData)
                adapter.notifyDataSetChanged()
            }
        })
    }

    private fun initMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private lateinit var map: GoogleMap
    private var useGps: Boolean = false
    private var marker: Marker? = null
    private var selectedCheckpoint: Checkpoint? = null
    private var selectedMarker: Marker? = null

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.isMyLocationEnabled = false

        setMapStyle()
        //Focus to place
        initPlace()
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(place!!.latitude!!, place!!.longitude!!), Config.DEFAULT_ZOOM))

        initPoints()
        initCheckpoints()

        true
    }

    private var place: Pos? = null

    private fun initPlace() {
        place = presenter.getMemberPlace()
        addPlaceMarker(place)
    }


    private fun initPoints() {
        pointPresenter.getPoints(false, object : FetchDataCallback {
            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(data: Any?) {
                var dataList = data as MutableList<TitikRonda>
                pointList.clear()
                pointList.addAll(dataList)
                addDotMarkers(dataList)
            }
        })
    }


    private fun addDotMarkers(list: MutableList<TitikRonda>) {
        pointList.clear()
        pointList.addAll(list)

        for (point in list) {
            val location = Location("serviceprovider")
            location.latitude = point.latitude!!
            location.longitude = point.longitude!!

            // Creating a marker
            val markerOptions = MarkerOptions().title(getString(R.string.label_point))
                    .icon(BitmapDescriptorFactory.fromBitmap(ResourceHelper.getBitmap(context, R.drawable.ic_dot)))
            val latlng = LatLng(location.latitude, location.longitude)
            markerOptions.position(latlng)

            var marker = map.addMarker(markerOptions)
        }
    }

    private fun setMapStyle() {
        val style = MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style)
        map.setMapStyle(style)
    }

    private fun addPlaceMarker(data: Pos?) {
        if (data?.latitude == null) return

        // Creating a marker
        val markerOptions = MarkerOptions().icon(
                BitmapDescriptorFactory.fromBitmap(ResourceHelper.getBitmap(this, R.drawable.ic_home_primary)))
                .title(data.name)
        val latlng = LatLng(data.latitude!!, data.longitude!!)
        markerOptions.position(latlng)
        map.addMarker(markerOptions)
    }

    private fun setMarker(location: Location?) {
        val latlng = LatLng(location!!.latitude, location.longitude)
        val markerOptions = MarkerOptions()
        markerOptions.position(latlng)
//        map.animateCamera(CameraUpdateFactory.newLatLng(latlng))
        marker?.remove()
        marker = map.addMarker(markerOptions)
    }

    companion object {

        fun startActivity(context: Context) {
            val intent = Intent(context, RondaAct::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
