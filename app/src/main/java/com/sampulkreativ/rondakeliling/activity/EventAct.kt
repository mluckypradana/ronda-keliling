package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.rri.customer.activity.core.CoreActivity
import com.rri.customer.helper.DateHelper
import com.rri.customer.helper.LocationHelper
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.model.Kejadian
import com.sampulkreativ.rondakeliling.presenter.EventPresenter
import com.sampulkreativ.rondakeliling.presenter.LocationPresenter
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import kotlinx.android.synthetic.main.act_event.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class EventAct : CoreActivity(), OnMapReadyCallback {
    override val viewRes: Int = R.layout.act_event
    val presenter: EventPresenter = EventPresenter()
    val locationPresenter = LocationPresenter()
    private var data: Kejadian? = null

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar(toolbar)
        initMap()
        b_proceed.setOnClickListener { showRoute() }
        initData()
    }

    private fun showRoute() {
        var member = MemberPresenter.getMember()
        locationPresenter.showMapsDirection(this, member?.latitude, member?.longitude,
                data?.latitude, data?.longitude)
    }


    private fun initData() {
        showLoading(this)
        val id = intent.getStringExtra("id")
        showLoading(this)
        presenter.getEvent(id, object : FetchDataCallback {
            override fun onFailure(message: String?) {
                hideLoading()
                Common.showToast(message)
                finish()
            }

            override fun onSuccess(response: Any?) {
                data = response as Kejadian?
                refreshData(data)
                hideLoading()
            }
        })
    }

    private fun refreshData(data: Kejadian?) {
        if (data == null) return

        val type = when (data.type) {
            Kejadian.OTHER -> data.description
            Kejadian.DISASTER -> App.context.getString(R.string.action_disaster)
            Kejadian.ACCIDENT -> App.context.getString(R.string.action_accident)
            Kejadian.FIRE -> App.context.getString(R.string.action_fire)
            Kejadian.THEFT -> App.context.getString(R.string.action_theft)
            else -> ""
        }

        tv_title.text = type
        tv_info.text = DateHelper.format(milis = data.createdAt)
        if(data.latitude != null) {
            setMarker(LocationHelper.createByDouble(data.latitude, data.longitude))
            //TODO Show route
        }
        else{
            ll_nav.visibility = View.GONE
        }

    }

    override fun showLoading(context: Context) {
        if (vf_main == null) return
        vf_main.displayedChild = if (data == null) 1 else 0
    }

    override fun hideLoading() {
        if (vf_main == null) return
        vf_main.displayedChild = if (data == null) 2 else 0
    }

    private lateinit var mapFragment: SupportMapFragment

    private fun initMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private lateinit var map: GoogleMap
    private var marker: Marker? = null

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.isMyLocationEnabled = false

        setMapStyle()
        LocationPresenter.setDefaultCamera(map)

        true
    }

    private fun setMapStyle() {
        val style = MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style)
        map.setMapStyle(style)
    }

    private fun setMarker(location: Location?) {
        if(location==null) return

        val latlng = LatLng(location!!.latitude, location.longitude)
        val markerOptions = MarkerOptions()
        markerOptions.position(latlng)

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 17F))
        marker?.remove()
        marker = map.addMarker(markerOptions)
    }

    companion object {

        fun startActivity(context: Context) {
            val intent = Intent(context, EventAct::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
