package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.activity.core.CoreActivity
import com.rri.customer.function.body
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import com.sampulkreativ.rondakeliling.presenter.PrefPresenter
import kotlinx.android.synthetic.main.act_login.*


class LoginAct : CoreActivity() {
    override val viewRes: Int = R.layout.act_login
    val presenter: MemberPresenter = MemberPresenter()
    var title = "Login"

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b_proceed.setOnClickListener { proceed() }
        title = PrefPresenter.getData() as String
        tv_title.text = title
    }


    private fun proceed() {
        showLoading(this)
        presenter.login(et_phone.body(), et_password.body(), object : PostDataCallback {
            override fun onValidation(message: String) {
                Common.showToast(message)
                hideLoading()
            }

            override fun onFailure(message: String?) {
                Common.showToast(message)
                hideLoading()
            }

            override fun onSuccess(data: Any?) {
                showMainPage()
                hideLoading()
            }
        })
    }

    private fun showMainPage() {
        MainAct.startActivity(this)
        finish()
    }

    private lateinit var mLastLocation: Location

}