package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.rri.customer.activity.core.CoreActivity
import com.rri.customer.helper.DateHelper
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.model.Pengumuman
import com.sampulkreativ.rondakeliling.presenter.AnnouncePresenter
import kotlinx.android.synthetic.main.act_announcement.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class AnnouncementAct : CoreActivity() {
    override val viewRes: Int = R.layout.act_announcement
    val presenter: AnnouncePresenter = AnnouncePresenter()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar(toolbar)
        initData()
    }

    private var data: Pengumuman? = null

    private fun initData() {
        showLoading(this)
        val id = intent.getStringExtra("id")
        presenter.getAnnouncement(id, object : FetchDataCallback {
            override fun onFailure(message: String?) {
                hideLoading()
                Common.showToast(message)
                finish()
            }

            override fun onSuccess(response: Any?) {
                data = response as Pengumuman?
                hideLoading()
                refreshData(data)
            }
        })
    }

    private fun refreshData(data: Pengumuman?) {
        if (data == null) return
        tv_title.text = data.title
        tv_description.text = data.description
        tv_info.text = DateHelper.format(milis = data.createdAt)
    }

    override fun showLoading(context: Context) {
        if (vf_main == null) return
        vf_main.displayedChild = if (data == null) 1 else 0
    }

    override fun hideLoading() {
        if (vf_main == null) return
        vf_main.displayedChild = if (data == null) 2 else 0
    }
}
