package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.rri.customer.activity.core.CoreActivity
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import com.sampulkreativ.rondakeliling.presenter.PrefPresenter
import kotlinx.android.synthetic.main.act_landing.*


class LandingAct : CoreActivity() {
    override val viewRes: Int = R.layout.act_landing
    val presenter: MemberPresenter = MemberPresenter()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tv_login.setOnClickListener {
            PrefPresenter.setData(getString(R.string.action_login_as_member))
            launchActivity(LoginAct::class.java)
        }
        b_login_admin.setOnClickListener {
            PrefPresenter.setData(getString(R.string.action_login_as_admin))
            launchActivity(LoginAct::class.java)
        }
        b_login_member.setOnClickListener {
            PrefPresenter.setData(getString(R.string.action_login_as_member))
            launchActivity(LoginAct::class.java)
        }
        b_register.setOnClickListener { launchActivity(RegisterAct::class.java) }
        b_register_member.setOnClickListener { launchActivity(RegisterMemberAct::class.java) }
//        initDatabase()
//        readFromDatabase()
    }

    companion object {

        fun startActivity(context: Context) {
            val intent = Intent(context, LandingAct::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
