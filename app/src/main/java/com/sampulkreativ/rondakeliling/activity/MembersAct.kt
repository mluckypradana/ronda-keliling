package com.sampulkreativ.rondakeliling.activity

import android.os.Bundle
import com.rri.customer.activity.core.CoreActivity
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.fragment.MembersFragment

/**
 * "Info Anggota" page
 * Use content from fragment
 * Use general layout activity_fragment
 */
class MembersAct : CoreActivity() {
    override val viewRes: Int = R.layout.act_fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showFragment(MembersFragment())
    }
}
