package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.os.Bundle
import com.rri.customer.activity.core.CoreActivity
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import kotlinx.android.synthetic.main.act_setting.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class SettingAct : CoreActivity() {
    override val viewRes: Int = R.layout.act_setting
    val presenter: MemberPresenter = MemberPresenter()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar(toolbar)

        b_logout.setOnClickListener { logout() }
        tv_feedback.setOnClickListener { showFeedbackPage() }
        initData()
    }

    private fun showFeedbackPage() {
//        launchActivity(FeedbackAct::class.java)
        launchActivity(FeedbackResultAct::class.java)
    }

    private fun initData() {
        val user = MemberPresenter.getMember() ?: return
        var place = MemberPresenter.getMemberPlace()
        tv_name.text = user.name
        tv_phone.text = user.phone
        tv_place.text = place?.name
        tv_code.text = MemberPresenter().getPasswordFromPos()
    }

    private fun logout() {
        presenter.logout()
        LandingAct.startActivity(this)
        finish()
    }

}
