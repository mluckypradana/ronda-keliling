package com.sampulkreativ.rondakeliling.activity

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.activity.core.CoreActivity
import com.rri.customer.function.body
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.app.App.Companion.context
import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.helper.ResourceHelper
import com.sampulkreativ.rondakeliling.listener.FetchLocationListener
import com.sampulkreativ.rondakeliling.model.Pos
import com.sampulkreativ.rondakeliling.presenter.LocationPresenter
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import com.sampulkreativ.rondakeliling.presenter.PosPresenter
import kotlinx.android.synthetic.main.act_register_2.*


class Register2Act : CoreActivity(), OnMapReadyCallback {
    override val viewRes: Int = R.layout.act_register_2
    val presenter: MemberPresenter = MemberPresenter()
    val placePresenter = PosPresenter()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b_proceed.setOnClickListener { proceed() }
        initMap()
    }


    private fun setMapStyle() {
        val style = MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style)
        map?.setMapStyle(style)
    }


    private fun initMap() {

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun proceed() {
        showLoading(this)
        presenter.registerAsRt2(et_place_name.body(), location, object : PostDataCallback {
            override fun onValidation(message: String) {
                Common.showToast(message)
                hideLoading()
            }

            override fun onFailure(message: String?) {
                Common.showToast(message)
                hideLoading()
            }

            override fun onSuccess(data: Any?) {
                Common.showToast(data as String)
                showMainPage()
                hideLoading()
            }
        })
    }

    private fun showMainPage() {
        MainAct.startActivity(context)
        finish()
    }

    private var map: GoogleMap? = null

    private var useGps: Boolean = false

    private var location: Location? = null

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        Dexter.withActivity(this)
                .withPermissions(arrayListOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION))
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        Common.showToast(R.string.message_permission_location)
                    }

                    @SuppressLint("MissingPermission")
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted()) {
                            map?.isMyLocationEnabled = true
                            map?.getMyLocation()
                            fetchCurrentLocation()
                        }
                    }
                }).check()

        setMapStyle()
        initPlaces()

        //Locate
//        setMarker(map.myLocation)
//        map?.isMyLocationEnabled = false
//        map.addMarker(MarkerOptions().position(latlng).title("Lokasi Poskamling"))
        LocationPresenter.setDefaultCamera(map)

        map?.setOnMapClickListener {
            useGps = false

            location = Location("serviceprovider")
            location!!.latitude = it.latitude
            location!!.longitude = it.longitude
            setMarker(location)
            myLocationEnabled = false
        }

        setupMyLocationFunction()
    }

    override fun onResume() {
        super.onResume()
        fetchCurrentLocation()
    }

    private var myLocationEnabled: Boolean = true

    private fun setupMyLocationFunction() {
        val myLocationChangeListener = GoogleMap.OnMyLocationChangeListener { location ->
            val loc = LatLng(location.latitude, location.longitude)
            if (myLocationEnabled) {
                map?.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, Config.DEFAULT_ZOOM))
                setMarker(location)
            }
        }
        map?.setOnMyLocationChangeListener(myLocationChangeListener)

        //add location button click listener
        map?.setOnMyLocationButtonClickListener {
            myLocationEnabled = true
            false
        }
    }

    private fun fetchCurrentLocation() {
        if (location != null) return

        Dexter.withActivity(this)
                .withPermissions(arrayListOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION))
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        Common.showToast(R.string.message_permission_location)
                    }

                    @SuppressLint("MissingPermission")
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted())
                            fetchLocationAfterPermission()
                    }
                }).check()



    }

    private fun fetchLocationAfterPermission() {
        LocationPresenter().fetchLocation(this, this, object : FetchLocationListener {
            override fun onFetchLocationSuccess(location: Location?) {
                this@Register2Act.location = location
                setMarker(if (map?.myLocation != null) map?.myLocation else location)
            }

            override fun onFetchLocationFailure() {
            }

            override fun onFetchLocationCanceled() {
            }
        })
    }

    private var marker: Marker? = null

    private fun setMarker(location: Location?) {
        val latlng = LatLng(location!!.latitude, location!!.longitude)
        val markerOptions = MarkerOptions()
        markerOptions.position(latlng)
//        map.animateCamera(CameraUpdateFactory.newLatLng(latlng))
        marker?.remove()
        marker = map?.addMarker(markerOptions)
        this.location = location
        fetchAddressDetail(location)
    }

    private fun initPlaces() {
        placePresenter.getPlaces(object : FetchDataCallback {
            override fun onFailure(message: String?) {
            }

            override fun onSuccess(data: Any?) {
                addPlaceMarkers(data as MutableList<Pos>)
            }
        })
    }


    private fun fetchAddressDetail(location: Location) {
        //Fetch address
        var address = LocationPresenter.fetchAddress(this, location)
        var text = address?.getAddressLine(0)
        if (!text.isNullOrEmpty()) {
            tv_info.text = text
            tv_info.visibility = View.VISIBLE
        } else
            tv_info.visibility = View.GONE

    }

    private fun addPlaceMarkers(list: MutableList<Pos>) {
        for (data in list) {
            if (data.latitude == null) return

            // Creating a marker
            val markerOptions = MarkerOptions().icon(
                    BitmapDescriptorFactory.fromBitmap(ResourceHelper.getBitmap(this, R.drawable.ic_home_primary)))
                    .title(data.name)
            val latlng = LatLng(data.latitude!!, data.longitude!!)
            markerOptions.position(latlng)
            map?.addMarker(markerOptions)
        }
    }
}