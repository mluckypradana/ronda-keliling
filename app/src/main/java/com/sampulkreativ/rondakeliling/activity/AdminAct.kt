package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.activity.core.CoreActivity
import com.rri.customer.helper.LocationHelper
import com.rri.customer.listener.OnItemClickListener
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.adapter.MemberAdminAdapter
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.helper.ResourceHelper
import com.sampulkreativ.rondakeliling.model.Member
import com.sampulkreativ.rondakeliling.model.Pos
import com.sampulkreativ.rondakeliling.model.temp.MemberMarker
import com.sampulkreativ.rondakeliling.presenter.AdminPresenter
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import com.sampulkreativ.rondakeliling.presenter.PosPresenter
import kotlinx.android.synthetic.main.act_admin.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class AdminAct : CoreActivity(), OnMapReadyCallback {
    override val viewRes: Int = R.layout.act_admin
    val presenter: MemberPresenter = MemberPresenter()
    val adminPresenter = AdminPresenter()
    val placePresenter = PosPresenter()
    private val list: MutableList<Member> = arrayListOf()
    private val memberMarkers: MutableList<MemberMarker> = arrayListOf()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar(toolbar)
        initList()
        initMap()
        initData()
    }

    private lateinit var adapter: MemberAdminAdapter

    private fun initList() {
        adapter = MemberAdminAdapter(list, object : OnItemClickListener {
            override fun onItemClicked(position: Int) {
                selectedMember = list[position]
                selectedMarker = selectedMember?.marker
//                map.animateCamera(CameraUpdateFactory.newLatLng(selectedMarker?.position))
                updateSelectedMember()
            }
        })
        rv_unassigned.layoutManager = LinearLayoutManager(App.context, LinearLayoutManager.VERTICAL, false)
        rv_unassigned.adapter = adapter
        adapter.addSeparator(rv_unassigned, R.dimen.card_space)
    }

    private fun updateSelectedMember() {
        tv_selected.text = if (selectedMember == null) "" else selectedMember!!.name
    }


    private fun initData() {
        b_set.setOnClickListener { setMemberLocation() }
    }

    private fun initMembers() {
        adminPresenter.getMembers(object : FetchDataCallback {
            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(data: Any?) {
                var listData = data as MutableList<Member>
                list.clear()
                list.addAll(listData)
                adapter.notifyDataSetChanged()

                addMemberMarkers()
            }
        })
    }

    private fun addMemberMarkers() {
        for (data in memberMarkers)
            data.marker?.remove()

        memberMarkers.clear()
        //Populate to list
        for (member in list) {
            val location = LocationHelper.createByDouble(member.latitude, member.longitude)
            var marker = addMemberMarker(member, location)
            marker?.let {
                var data = MemberMarker(member, marker)
                memberMarkers.add(data)
            }
        }
    }

    private fun setMemberLocation() {
        adminPresenter.setMemberLocation(selectedMember, location, object : PostDataCallback {
            override fun onValidation(message: String) {
                Common.showToast(message)
            }

            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(data: Any?) {
                selectedMarker?.position = LatLng(location!!.longitude, location!!.longitude)
                marker?.remove()
                location = null
                updateSelectedMember()
            }
        })
    }

    private fun addMemberMarker(member: Member?, location: Location?): Marker? {
        if (member == null || location == null) return null

        // Creating a marker
        val markerOptions = MarkerOptions().title(member.name)
                .icon(BitmapDescriptorFactory.fromBitmap(ResourceHelper.getBitmap(this, R.drawable.ic_radio_white)))
        val latlng = LatLng(location.latitude, location.longitude)
        markerOptions.position(latlng)
        return map.addMarker(markerOptions)
//        member.marker = memberMarker
    }

    private fun proceed() {
        if (list.size == 0) {
            Common.showToast(R.string.error_empty_point)
            return
        }
        launchActivity(MainAct::class.java)
        finish()
    }

    private fun initMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private lateinit var map: GoogleMap
    private var useGps: Boolean = false
    private var marker: Marker? = null
    private var selectedMember: Member? = null
    private var selectedMarker: Marker? = null

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.isMyLocationEnabled = true

        setMapStyle()
        //Focus to place
        initPlaces()

        val defaultLocation = LatLng(-6.873586, 107.561796)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLocation, 12F))
//        LocationPresenter.setDefaultCamera(map)

        map.setOnMapClickListener {
            useGps = false

            location = Location("serviceprovider")
            location!!.latitude = it.latitude
            location!!.longitude = it.longitude

            // Creating a marker
            setMarker(location)
        }

        map.setOnMarkerClickListener {
            var mark = it
            var memberMarker = memberMarkers.find { it.marker == mark }
            if (memberMarker != null) {
                selectedMarker = memberMarker.marker
                updateSelectedMember()
            } else
                location = LocationHelper.createByDouble(it.position.latitude, it.position.longitude)

            false
        }
        initMembers()

        true
    }

    private var location: Location? = null


    private fun zoomCamera() {
        map.animateCamera(CameraUpdateFactory.zoomTo(Config.DEFAULT_ZOOM))
    }

    private fun setMapStyle() {
        val style = MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style)
        map.setMapStyle(style)
    }


    private val places: MutableList<Pos> = arrayListOf()

    private fun initPlaces() {
        placePresenter.getPlaces(object : FetchDataCallback {
            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(response: Any?) {
                var list = response as MutableList<Pos>
                places.clear()
                places.addAll(list)
                adapter.notifyDataSetChanged()

                //Populate to list
                addPlaceMarkers(places)
            }
        })
    }

    private fun addPlaceMarkers(list: MutableList<Pos>) {
        for (data in list) {
            if (data.latitude == null) return

            // Creating a marker
            val markerOptions = MarkerOptions().icon(
                    BitmapDescriptorFactory.fromBitmap(ResourceHelper.getBitmap(this, R.drawable.ic_home_primary)))
                    .title(data.name)
            val latlng = LatLng(data.latitude!!, data.longitude!!)
            markerOptions.position(latlng)
            map.addMarker(markerOptions)
        }
    }

    private fun setMarker(location: Location?) {
        val latlng = LatLng(location!!.latitude, location.longitude)
        val markerOptions = MarkerOptions()
        markerOptions.position(latlng)
//        map.animateCamera(CameraUpdateFactory.newLatLng(latlng))
        marker?.remove()
        marker = map.addMarker(markerOptions)
    }

    companion object {

        fun startActivity(context: Context) {
            val intent = Intent(context, AdminAct::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
