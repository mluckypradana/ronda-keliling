package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.location.Location
import android.os.Bundle
import android.os.IBinder
import android.os.Looper
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.kopnus.kdigi.presenter.callback.RondaStateCallback
import com.rri.customer.activity.core.CoreActivity
import com.sampulkreativ.rondakeliling.MyService
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.fragment.HistoryFragment
import com.sampulkreativ.rondakeliling.fragment.Intro2DialogFragment
import com.sampulkreativ.rondakeliling.fragment.IntroDialogFragment
import com.sampulkreativ.rondakeliling.fragment.MainFragment
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.listener.OnClickListener
import com.sampulkreativ.rondakeliling.model.Member
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import com.sampulkreativ.rondakeliling.presenter.PointPresenter
import com.sampulkreativ.rondakeliling.presenter.PrefPresenter
import kotlinx.android.synthetic.main.act_main.*


class MainAct : CoreActivity() {

    private val TAG = "LocationActivity"
    private val INTERVAL = (1000 * 5).toLong()
    private val FASTEST_INTERVAL = (1000 * 10).toLong()
    lateinit var mLocationRequest: LocationRequest
    var mCurrentLocation: Location? = null
    var mLastUpdateTime: String? = null
    private lateinit var mFusedLocationClient: FusedLocationProviderClient

    private lateinit var locationCallback: LocationCallback
    private lateinit var mGoogleApiClient: GoogleApiClient

    companion object {

        fun startActivity(context: Context) {
            val intent = Intent(context, MainAct::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }

    override val viewRes: Int = com.sampulkreativ.rondakeliling.R.layout.act_main

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            com.sampulkreativ.rondakeliling.R.id.navigation_ronda -> {
                showRondaPage()
                return@OnNavigationItemSelectedListener true
            }
            com.sampulkreativ.rondakeliling.R.id.navigation_dashboard -> {
                showHistoryPage()
                return@OnNavigationItemSelectedListener true
            }
//            R.id.navigation_member -> {
//                showMembersPage()
//                return@OnNavigationItemSelectedListener true
//            }
        }
        false
    }

    private var historyFragment = HistoryFragment()
    private var rondaFragment = MainFragment()

    fun showMembersPage() {
        launchActivity(MembersAct::class.java)
//        showFragment(membersFragment)
    }

    private fun showRondaPage() {
//        showFragment(rondaFragment)

        var transaction = supportFragmentManager.beginTransaction()
        transaction.show(rondaFragment)
        transaction.hide(historyFragment)
        transaction.commit()
    }

    private fun showHistoryPage() {
//        showFragment(historyFragment)

        var transaction = supportFragmentManager.beginTransaction()
        transaction.hide(rondaFragment)
        transaction.show(historyFragment)
        transaction.commit()
    }

    private var settingItem: MenuItem? = null

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(com.sampulkreativ.rondakeliling.R.menu.setting, menu)

        settingItem = menu?.findItem(R.id.m_setting)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.m_setting -> showAddPointPage()
            R.id.m_member -> showMembersPage()
            R.id.m_info -> showSettingPage()
        }

        return super.onOptionsItemSelected(item)
    }

    fun showMainFragment(fragment: Fragment) {
        var transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.ll_container, fragment)
        //TODO Change to replace()


        //Dont stack first fragment
//        transaction.addToBackStack(fragment::class.java.name)

        transaction.commit()
        fragmentList.add(fragment)
    }

    fun showSettingPage() {
        launchActivity(SettingAct::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showRondaPage()
        navigation.selectedItemId = R.id.navigation_ronda
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

//        initToolbar(toolbar)
        initData()

        if (PrefPresenter.shouldShowIntro()) {
            PrefPresenter.setShowedIntro()
            showIntroDialog()
            return
        }

        startMyService()
    }

    private fun showIntroDialog() {
        var fragment = IntroDialogFragment()
        fragment.listener = object : OnClickListener {
            override fun onClicked() {
                fragment.dismiss()
                showNextIntroDialog()
            }
        }
        fragment.show(supportFragmentManager, null)
    }

    private fun showNextIntroDialog() {
        var fragment = Intro2DialogFragment()
        fragment.listener = object : OnClickListener {
            override fun onClicked() {
                fragment.dismiss()
                showAddPointPage()
            }
        }
        fragment.show(supportFragmentManager, null)
    }

    private val memberPresenter = MemberPresenter()
    private val pointPresenter = PointPresenter()
    private var memberTotal: Int = 0
    private var pointTotal: Int = 0

    private var member: Member? = null

    fun initData() {
        showMainFragment(historyFragment)
        showMainFragment(rondaFragment)

        member = MemberPresenter.getMember()
        if (member == null) {
            MemberPresenter().logout()
            finish()
            return
        }

        if (!member!!.placeAdmin)
            settingItem?.isVisible = false

        memberPresenter.checkRondaState(object : RondaStateCallback {
            override fun onMemberEmpty() {
//                showMembersPage()
//                finish()
            }

            override fun onPointEmpty() {
//                showAddPointPage()
//                finish()
            }

            override fun onRondaAvailable() {
//                showRondaPage()
            }
        })

    }

    private fun showAddPointPage() {
        member = MemberPresenter.getMember()

        if (member == null) {
            MemberPresenter().logout()
            finish()
            return
        }

        if (!member?.placeAdmin!!) {
            Common.showToast(R.string.error_need_pengurus)
            return
        }

        launchActivity(PointAct::class.java)
    }

    private var m_service: MyService? = null
    private val m_serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            m_service = (service as MyService.MyBinder).service
        }

        override fun onServiceDisconnected(className: ComponentName) {
            m_service = null
        }
    }

    private var serviceIntent: Intent? = null

    private lateinit var service: Intent

    private fun startMyService() {
// use this to start and trigger a service
        service = Intent(baseContext, MyService::class.java)
// potentially add data to the intent
        service.putExtra("KEY1", "Value to be used by the service")
        startService(service)
    }

    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = INTERVAL
        mLocationRequest.fastestInterval = FASTEST_INTERVAL
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        mLocationRequest.smallestDisplacement = Config.UPDATE_DISTANCE
    }

    @SuppressLint("MissingPermission")
    protected fun startLocationUpdates() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                val locationList = locationResult?.locations
                var size = locationList?.size ?: 0
                if (size > 0)
                    locationList?.get(0)?.let {
                        MemberPresenter.updateMemberLocation(it)
//                        addNotification(it)
                    }
            }
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, locationCallback, Looper.myLooper())
        Log.d(TAG, "Location update started ..............: ")
    }

    override fun onDestroy() {
        MemberPresenter.updateMemberLocation(null)
        serviceIntent?.let { stopService(it) }
        super.onDestroy()
    }
}