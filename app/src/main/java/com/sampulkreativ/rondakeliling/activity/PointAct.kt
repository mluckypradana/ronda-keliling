package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.activity.core.CoreActivity
import com.rri.customer.api.callback.PointEvent
import com.rri.customer.api.callback.QrCodeEvent
import com.rri.customer.helper.LocationHelper
import com.rri.customer.listener.OnItemClickListener
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.config.Config.DEFAULT_ZOOM
import com.sampulkreativ.rondakeliling.fragment.AddQrCodeDialogFragment
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.helper.ResourceHelper
import com.sampulkreativ.rondakeliling.listener.FetchLocationListener
import com.sampulkreativ.rondakeliling.model.Pos
import com.sampulkreativ.rondakeliling.model.TitikRonda
import com.sampulkreativ.rondakeliling.presenter.LocationPresenter
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import com.sampulkreativ.rondakeliling.presenter.PointPresenter
import com.sampulkreativ.rondakeliling.presenter.PosPresenter
import kotlinx.android.synthetic.main.act_point.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class PointAct : CoreActivity(), OnMapReadyCallback {
    override val viewRes: Int = R.layout.act_point
    val presenter: MemberPresenter = MemberPresenter()
    val placePresenter = PosPresenter()
    var pointChanged = false
    val pointPresenter = PointPresenter()
    private val list: MutableList<TitikRonda> = arrayListOf()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b_set.setOnClickListener { showScanDialog() }
        b_proceed.setOnClickListener { proceed() }
        b_remove.setOnClickListener { removePoint() }
        initToolbar(toolbar)
        initMap()
    }

    override fun onDestroy() {
        if (pointChanged)
            EventBus.getDefault().postSticky(PointEvent())

        super.onDestroy()
    }

    private fun removePoint() {
        AlertDialog.Builder(this)
                .setTitle(getString(R.string.action_confirm_delete))
                .setMessage(getString(R.string.message_confirm_delete))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.action_yes) { dialog, which ->
                    proceedRemovePoint()
                }.setNegativeButton(R.string.action_no) { dialog, which ->
                    dialog.dismiss()
                }.show()
    }

    private fun proceedRemovePoint() {
        pointPresenter.removePoint(selectedPoint, object : PostDataCallback {
            override fun onValidation(message: String) {
                Common.showToast(message)
            }

            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(data: Any?) {
                pointChanged = true
                selectedMarker?.remove()
            }
        })
    }

    private fun initPoint() {
        pointPresenter.getPoints(object : FetchDataCallback {
            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(data: Any?) {
                var listData = data as MutableList<TitikRonda>
                list.clear()
                list.addAll(listData)

                //Populate to list
                for (point in list) {
                    val location = LocationHelper.createByDouble(point.latitude, point.longitude)
                    addDotMarker(point, location)
                }

                pointChanged = true
            }
        })
    }


    //Redirect to order page with eventbus
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onEvent(event: QrCodeEvent) {
        EventBus.getDefault().removeStickyEvent(event)
        setPoint(event.text)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    private fun showScanDialog() {
        if (location == null && selectedPoint == null) {
            Common.showToast(R.string.error_empty_location)
            return
        }

        Common.showMessageDialog(context = this, message = getString(R.string.message_define_qr_code),
                dismissListener = DialogInterface.OnDismissListener {
                    launchActivity(SimpleScannerActivity::class.java)
                })
        return


        var fragment = AddQrCodeDialogFragment()
        fragment.listener = object : OnItemClickListener {
            override fun onItemClicked(position: Int) {
                setPoint(fragment.text)
                fragment.dismiss()
            }
        }
        fragment.show(supportFragmentManager, null)
    }

    private fun setPoint(text: String?) {
        if (selectedPoint != null) {
            pointPresenter.updatePoint(selectedPoint!!, text, null)
            Common.showToast(R.string.message_success_update_point)
            selectedPoint = null
            updateActionState()
        } else
            pointPresenter.registerPoint(text, location, object : PostDataCallback {
                override fun onValidation(message: String) {
                    Common.showToast(message)
                }

                override fun onFailure(message: String?) {
                    Common.showToast(message)
                }

                override fun onSuccess(response: Any?) {
                    var data = response as TitikRonda
                    addDotMarker(data, location)
                    marker?.remove()
                    list.add(data)

                    location= null
                    updateActionState()
                    pointChanged = true
                }
            })
    }

    private fun addDotMarker(point: TitikRonda?, location: Location?) {
        // Creating a marker
        val markerOptions = MarkerOptions().title(getString(R.string.label_point))
                .icon(BitmapDescriptorFactory.fromBitmap(ResourceHelper.getBitmap(this, R.drawable.ic_dot)))
        val latlng = LatLng(location!!.latitude, location.longitude)
        markerOptions.position(latlng)
        var marker = map.addMarker(markerOptions)
        marker.tag = point
    }

    private fun proceed() {
        if (list.size == 0) {
            Common.showToast(R.string.error_empty_point)
            return
        }
        launchActivity(MainAct::class.java)
        finish()
    }

    private fun initMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private lateinit var map: GoogleMap
    private var myLocationEnabled: Boolean = true
    private var marker: Marker? = null
    private var selectedPoint: TitikRonda? = null
    private var selectedMarker: Marker? = null

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map?.isMyLocationEnabled = true

        setMapStyle()
        //Focus to place
        initPlace()
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(place!!.latitude!!, place!!.longitude!!), DEFAULT_ZOOM))

        map.setOnMapClickListener {
            location = LocationHelper.createByDouble(it.latitude, it.longitude)
            // Creating a marker
            setMarker(location)

            selectedPoint = null
            updateActionState()
            myLocationEnabled = false
        }

        map.setOnMarkerClickListener {
            selectedMarker = it
            if (it.tag != null && it.tag is TitikRonda)
                selectedPoint = it.tag as TitikRonda
            else
                selectedPoint = null
            updateActionState()
            false
        }


        setupMyLocationFunction()
        initPoint()
//        fetchCurrentLocation()
        true
    }

    private fun setupMyLocationFunction() {
        val myLocationChangeListener = GoogleMap.OnMyLocationChangeListener { location ->
            val loc = LatLng(location.latitude, location.longitude)
            if (myLocationEnabled) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, Config.DEFAULT_ZOOM))
                setMarker(location)
            }
        }
        map.setOnMyLocationChangeListener(myLocationChangeListener)

        //add location button click listener
        map.setOnMyLocationButtonClickListener {
            myLocationEnabled = true
            false
        }
    }

    private fun updateActionState() {
        if (selectedPoint != null) {
            b_set.setText(R.string.action_change_qr_code)
            b_remove.visibility = View.VISIBLE
        } else {
            b_set.setText(R.string.action_set_as_point)
            b_remove.visibility = View.GONE
        }
    }

    private var location: Location? = null

    private fun fetchCurrentLocation() {
        if (location != null) return

//        showLoading(this)
        LocationPresenter().fetchLocation(this, this, object : FetchLocationListener {
            override fun onFetchLocationSuccess(location: Location?) {
                this@PointAct.location = location
                zoomCamera()
                setMarker(if (map.myLocation != null) map.myLocation else location)
                marker?.remove()

//                hideLoading()
            }

            override fun onFetchLocationFailure() {
//                Common.showToast(R.string.error_empty_location)
                hideLoading()
            }

            override fun onFetchLocationCanceled() {
                hideLoading()

                //Zoom to pos
                val defaultLocation = LatLng(place!!.latitude!!, place!!.longitude!!)
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLocation, DEFAULT_ZOOM))
            }
        })
    }

    private fun zoomCamera() {
        map.animateCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM))
    }

    private fun setMapStyle() {
        val style = MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style)
        map.setMapStyle(style)
    }

    private var place: Pos? = null

    private fun initPlace() {
        place = presenter.getMemberPlace()
        addPlaceMarkers(arrayListOf(place))
    }

    private fun addPlaceMarkers(list: MutableList<Pos?>) {
        for (data in list) {
            if (data?.latitude == null) return

            // Creating a marker
            val markerOptions = MarkerOptions().icon(
                    BitmapDescriptorFactory.fromBitmap(ResourceHelper.getBitmap(this, R.drawable.ic_home_primary)))
                    .title(data.name)
            val latlng = LatLng(data.latitude!!, data.longitude!!)
            markerOptions.position(latlng)
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, DEFAULT_ZOOM))
            map.addMarker(markerOptions)
        }
    }

    private var markerOptions: MarkerOptions? = null

    private fun setMarker(location: Location?) {
        val latlng = LatLng(location!!.latitude, location!!.longitude)
        val markerOptions = MarkerOptions()
        markerOptions.position(latlng)
//        map.animateCamera(CameraUpdateFactory.newLatLng(latlng))
        marker?.remove()
        marker = map.addMarker(markerOptions)
        this.location = location
    }
}