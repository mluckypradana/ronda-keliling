package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.activity.core.CoreActivity
import com.rri.customer.function.body
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.presenter.AdminPresenter
import kotlinx.android.synthetic.main.act_admin_login.*


class AdminLoginAct : CoreActivity() {
    override val viewRes: Int = R.layout.act_admin_login
    val presenter: AdminPresenter = AdminPresenter()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        initDatabase()
//        readFromDatabase()
        b_proceed.setOnClickListener { proceed() }
    }

    private fun proceed() {
        presenter.login(et_username.body(), et_password.body(), object : PostDataCallback {
            override fun onValidation(message: String) {
                Common.showToast(message)
            }

            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(data: Any?) {
                showMainPage()
            }
        })
    }

    private fun showMainPage() {
        AdminAct.startActivity(this)
    }
    companion object {

        fun startActivity(context: Context) {
            val intent = Intent(context, AdminLoginAct::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
