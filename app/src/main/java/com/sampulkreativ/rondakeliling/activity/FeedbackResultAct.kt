package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.os.Bundle
import com.rri.customer.activity.core.CoreActivity
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import kotlinx.android.synthetic.main.act_feedback_result.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class FeedbackResultAct : CoreActivity() {
    override val viewRes: Int = R.layout.act_feedback_result
    val presenter: MemberPresenter = MemberPresenter()
    var values = arrayOf(
            intArrayOf(5, 4, 4, 4, 5),
            intArrayOf(4, 4, 5, 4, 5),
            intArrayOf(5, 4, 5, 4, 3),
            intArrayOf(3, 5, 5, 4, 4),
            intArrayOf(3, 4, 5, 5, 5),
            intArrayOf(4, 3, 5, 3, 4),
            intArrayOf(4, 5, 5, 4, 5),
            intArrayOf(4, 4, 4, 4, 5)
    )

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar(toolbar)
        tv_total.text = values.size.toString() + " tanggapan"

        var t15 = 0.0
        var t14 = 0.0
        var t13 = 0.0
        var t12 = 0.0
        var t11 = 0.0
        var t25 = 0.0
        var t24 = 0.0
        var t23 = 0.0
        var t22 = 0.0
        var t21 = 0.0
        var t35 = 0.0
        var t34 = 0.0
        var t33 = 0.0
        var t32 = 0.0
        var t31 = 0.0
        var t45 = 0.0
        var t44 = 0.0
        var t43 = 0.0
        var t42 = 0.0
        var t41 = 0.0
        var t55 = 0.0
        var t54 = 0.0
        var t53 = 0.0
        var t52 = 0.0
        var t51 = 0.0
        for (value in values) {
//            for (valu in value)
            if (value[0] == 5) t15++
            if (value[1] == 5) t25++
            if (value[2] == 5) t35++
            if (value[3] == 5) t45++
            if (value[4] == 5) t55++

            if (value[0] == 4) t14++
            if (value[1] == 4) t24++
            if (value[2] == 4) t34++
            if (value[3] == 4) t44++
            if (value[4] == 4) t54++

            if (value[0] == 3) t13++
            if (value[1] == 3) t23++
            if (value[2] == 3) t33++
            if (value[3] == 3) t43++
            if (value[4] == 3) t53++

            if (value[0] == 2) t12++
            if (value[1] == 2) t22++
            if (value[2] == 2) t32++
            if (value[3] == 2) t42++
            if (value[4] == 2) t52++

            if (value[0] == 1) t11++
            if (value[1] == 1) t21++
            if (value[2] == 1) t31++
            if (value[3] == 1) t41++
            if (value[4] == 1) t51++
        }

        var b5: Int = (t15 + t25 + t35 + t45 + t55).toInt()
        var b4: Int = (t14 + t24 + t34 + t44 + t54).toInt()
        var b3: Int = (t13 + t23 + t33 + t43 + t53).toInt()
        var b2: Int = (t12 + t22 + t32 + t42 + t52).toInt()
        var b1: Int = (t11 + t21 + t31 + t41 + t51).toInt()

        t_b_5.text = "5 = $b5"
        t_b_4.text = "4 = $b4"
        t_b_3.text = "3 = $b3"
        t_b_2.text = "2 = $b2"
        t_b_1.text = "1 = $b1"
        var nilaiAkhir: Double = ((b5 * 5) + (b4 * 4) + (b3 * 3) + (b2 * 2) + (b1 * 1)).toDouble()
        var totalPertanyaan = 5
        var tertinggi = 5 * values.size.toDouble() * totalPertanyaan
        var persentase = nilaiAkhir * 100 / tertinggi
        tv_bobot.text = "Total bobot = ${nilaiAkhir.toInt()}"
        tv_tertinggi.text = "Bobot tertinggi = ${tertinggi.toInt()}"
        tv_interpretation.text = "Skor hasil pengamatan = $persentase%"

        var total = values.size.toDouble()
        val p15 = t15 / total * 100
        val p14 = t14 / total * 100
        val p13 = t13 / total * 100
        val p12 = t12 / total * 100
        val p11 = t11 / total * 100

        val p25 = t25 / total * 100
        val p24 = t24 / total * 100
        val p23 = t23 / total * 100
        val p22 = t22 / total * 100
        val p21 = t21 / total * 100

        val p35 = t35 / total * 100
        val p34 = t34 / total * 100
        val p33 = t33 / total * 100
        val p32 = t32 / total * 100
        val p31 = t31 / total * 100

        val p45 = t45 / total * 100
        val p44 = t44 / total * 100
        val p43 = t43 / total * 100
        val p42 = t42 / total * 100
        val p41 = t41 / total * 100

        val p55 = t55 / total * 100
        val p54 = t54 / total * 100
        val p53 = t53 / total * 100
        val p52 = t52 / total * 100
        val p51 = t51 / total * 100

        t_1_5.text = "5 = $p15%"
        t_1_4.text = "4 = $p14%"
        t_1_3.text = "3 = $p13%"
        t_1_2.text = "2 = $p12%"
        t_1_1.text = "1 = $p11%"

        t_2_5.text = "5 = $p25%"
        t_2_4.text = "4 = $p24%"
        t_2_3.text = "3 = $p23%"
        t_2_2.text = "2 = $p22%"
        t_2_1.text = "1 = $p21%"

        t_3_5.text = "5 = $p35%"
        t_3_4.text = "4 = $p34%"
        t_3_3.text = "3 = $p33%"
        t_3_2.text = "2 = $p32%"
        t_3_1.text = "1 = $p31%"

        t_4_5.text = "5 = $p45%"
        t_4_4.text = "4 = $p44%"
        t_4_3.text = "3 = $p43%"
        t_4_2.text = "2 = $p42%"
        t_4_1.text = "1 = $p41%"

        t_5_5.text = "5 = $p55%"
        t_5_4.text = "4 = $p54%"
        t_5_3.text = "3 = $p53%"
        t_5_2.text = "2 = $p52%"
        t_5_1.text = "1 = $p51%"
    }

    private fun proceed() {

    }
}
