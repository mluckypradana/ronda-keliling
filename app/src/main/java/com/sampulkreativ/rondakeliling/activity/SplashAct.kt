package com.sampulkreativ.rondakeliling.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import com.rri.customer.activity.core.CoreActivity
import com.sampulkreativ.rondakeliling.BuildConfig
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.presenter.AdminPresenter
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import com.sampulkreativ.rondakeliling.presenter.PrefPresenter
import kotlinx.android.synthetic.main.act_splash.*

class SplashAct : CoreActivity() {
    override val viewRes: Int = R.layout.act_splash
    val presenter: MemberPresenter = MemberPresenter()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        enableFullscreen()
        initSplashScreen()
//        FeedbackPresenter().putFeedback()
        tv_version.text = "Versi: " + BuildConfig.VERSION_NAME
    }

    private fun enableFullscreen() {
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//        val attrib = window.attributes
//        attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
    }

    private fun initSplashScreen() {
        val handler = Handler()
        handler.postDelayed({

            //Check pref version
            PrefPresenter.checkVersion()


            if (BuildConfig.FLAVOR == "admin") {
                if (AdminPresenter().isAdminLoggedIn())
                    AdminAct.startActivity(this)
                else
                    AdminLoginAct.startActivity(this)
            }
            //Demo or production
            else {
                //Check user
                if (presenter.getMember() != null)
                    MainAct.startActivity(this)
                else
                    LandingAct.startActivity(this)
            }

            finish()
        }, resources.getInteger(R.integer.splash_delay).toLong())
    }
}
