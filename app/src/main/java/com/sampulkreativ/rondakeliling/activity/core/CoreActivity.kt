package com.rri.customer.activity.core

import android.content.Context
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.Toolbar
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.helper.IntentHelper


/**
 * Created by Alvin Rusli on 06/07/2017.
 *
 * The parent activity, all other activities **should** extend from this class.
 */
abstract class CoreActivity : AppCompatActivity() {
    val fragmentList: MutableList<Fragment> = arrayListOf()

    companion object {
        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }
    }

    /**
     * The layout res for the current activity.
     * @return the layout resource ID, return a null to make the activity not set a content view.
     */
    abstract val viewRes: Int?

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//
//        Typeface.BOLD
//        //Set to default font
//        FontHelper.setDefaultFont(this, "MONOSPACE", getString(R.string.font_bold))
//        FontHelper.setDefaultFont(this, "SANS_SERIF", getString(R.string.font_default))
//        FontHelper.setDefaultFont(this, "SERIF", getString(R.string.font_default))
//        FontHelper.setDefaultFont(this, "DEFAULT", getString(R.string.font_default))

        //Init crash handler
//        ExceptionHandler.init(this, CrashActivity::class.java)

        if (viewRes != null) setContentView(viewRes!!)
    }

    /** Show wait progress dialog with general message */
    open fun showLoading(context: Context) {
        Common.showProgressDialog(context, R.string.message_wait, null)
    }

    /** Hide wait progress dialog with */
    open fun hideLoading() {
        Common.dismissProgressDialog()
    }

//    override fun attachBaseContext(newBase: Context) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
//    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        if (fragmentList.size > 1)
            fragmentList.removeAt(fragmentList.size - 1)
        else if (fragmentList.size == 1) {
            finish()
            return
        }
        super.onBackPressed()
    }

    open fun initToolbar(toolbar: Toolbar) {
        initToolbar(toolbar, true)
    }

    /** Set support actionbar, add back button */
    open fun initToolbar(toolbar: Toolbar, showBackButton: Boolean) {
        setSupportActionBar(toolbar)
        if (showBackButton)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)

//        FontHelper.applyFontForToolbarTitle(toolbar)
//        val actionBar = supportActionBar
//        actionBar?.setDisplayShowHomeEnabled(false)
//        actionBar?.setDisplayShowCustomEnabled(true)
////        actionBar?.setDisplayShowTitleEnabled(false)

//        val parent = toolbar
//        parent.setPadding(0, 0, 0, 0)//for tab otherwise give space in tab
//        parent.setContentInsetsAbsolute(0, 0)
    }


    open fun showFragment(fragment: Fragment?) {
        if (fragment == null) return

        //Hide current fragment
        var currentFragment =
                if (fragmentList.size == 0) null
                else fragmentList[fragmentList.size - 1]//getCurrentFragment()

        //Dont show fragment twice
        if (fragment == currentFragment) return

        val transaction = supportFragmentManager.beginTransaction()

        var find = fragmentList.find { it == fragment }

        if (find == null) {
            transaction.add(R.id.ll_container, fragment)
            //TODO Change to replace()

            if (currentFragment != null)
                transaction.hide(currentFragment)

            //Dont stack first fragment
            transaction.addToBackStack(fragment.javaClass.name)

            transaction.commit()
            fragmentList.add(fragment)
        } else {
            //Show added fragment
//            transaction.remove(fragment)
//            transaction.add(R.id.ll_container, fragment)
//
//            transaction.hide(currentFragment)
//            transaction.commit()
//            fragmentList.add(fragment)
            transaction.show(fragment)
        }
    }

    internal fun launchActivity(java: Class<*>) {
        IntentHelper.launch(this, java)
    }
}
