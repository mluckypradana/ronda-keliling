package com.rri.customer.config

/**
 * Created by MuhammadLucky on 05/02/2018.
 */

object Db {
    const val POS = "pos"
    const val MEMBERS = "member"
    const val EVENTS = "kejadian"
    const val FEEDBACKS = "feedback"
    const val ANNOUNCEMENTS = "pengumuman"
    const val RONDA = "ronda"
    const val POINTS = "titik"
    const val CHECKPOINTS = "checkpoint"
}
