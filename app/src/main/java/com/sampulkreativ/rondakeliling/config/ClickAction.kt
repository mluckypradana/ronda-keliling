package com.rri.customer.config

/**
 * Created by MuhammadLucky on 05/02/2018.
 */

object ClickAction {
    const val POS = "POS"
    const val MEMBERS = "MEMBERS"
    const val EVENTS = "EVENTS"
    const val ANNOUNCEMENTS = "ANNOUNCEMENTS"
    const val RONDA = "RONDA"
    const val POINTS = "POINTS"
    const val CHECKPOINTS = "CHECKPOINTS"
}
