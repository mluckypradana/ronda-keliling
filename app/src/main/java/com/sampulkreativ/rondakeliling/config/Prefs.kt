package com.rri.customer.config

/**
 * Created by MuhammadLucky on 05/02/2018.
 */

object Prefs {
    const val PREF_VERSION = "PREF_VERSION"
    const val USER = "USER"
    const val PLACE = "PLACE"
    const val HOME_CONTENT = "HOME_CONTENT"
    const val ORDER_PRODUCTS = "ORDER_PRODUCTS"
    const val TEMP_DATA = "TEMP_DATA"
    const val LOGIN_AS_ADMIN = "LOGIN_AS_ADMIN"
    const val SHOW_INTRO = "SHOW_INTRO"
}
