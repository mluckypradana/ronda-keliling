package com.rri.customer.config

/**
 * Created by MuhammadLucky on 05/02/2018.
 */

object Extras {
    const val IMAGE = "IMAGE"
    const val TITLE = "TITLE"
    const val UPLOAD_LABEL = "UPLOAD_LABEL"
    const val ENABLE_PIN = "ENABLE_PIN"
}
