package com.rri.customer.function

import android.view.View
import android.widget.EditText

/**
 * Extension function
 * Created by MuhammadLucky on 19/04/2018.
 */

fun EditText.body(): String = text.toString().trim()

fun EditText.setInnerHint(hint: String) {
    val editText = this
    editText.onFocusChangeListener = View.OnFocusChangeListener { _, b ->
        editText.hint = if (b) hint else ""
    }
}