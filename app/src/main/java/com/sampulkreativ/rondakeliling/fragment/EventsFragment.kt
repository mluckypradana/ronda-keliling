package com.sampulkreativ.Kejadiankeliling.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.rri.customer.fragment.core.CoreFragment
import com.rri.customer.listener.OnItemClickListener
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.activity.EventAct
import com.sampulkreativ.rondakeliling.activity.PointAct
import com.sampulkreativ.rondakeliling.adapter.EventAdapter
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.model.Kejadian
import com.sampulkreativ.rondakeliling.presenter.EventPresenter
import kotlinx.android.synthetic.main.fragment_events.*
import kotlinx.android.synthetic.main.layout_no_data.*

/**
 * Fragment for content in
 * Created by MuhammadLucky on 1/25/2017.
 */
class EventsFragment : CoreFragment() {

    private var presenter = EventPresenter()
    private val list: MutableList<Kejadian> = arrayListOf()
    private lateinit var adapter: EventAdapter

    override val viewRes: Int? = R.layout.fragment_events

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        initView()
        initData()
    }

    private var pointTotal: Int = 0

    private fun initData() {

        showLoading(context!!)
        presenter.getEvents(object : FetchDataCallback {
            override fun onFailure(message: String?) {
                Common.showToast(message)
                hideLoading()
            }

            override fun onSuccess(data: Any?) {
                refreshData(data as MutableList<Kejadian>)
                hideLoading()
            }
        })
    }

    private fun refreshData(list: MutableList<Kejadian>) {
        this.list.clear()
        this.list.addAll(list)
        adapter.notifyDataSetChanged()
    }

//    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
//        // Do something that differs the Activity's menu here
//        inflater?.inflate(R.menu.setting, menu);
//        super.onCreateOptionsMenu(menu, inflater)
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        when (item!!.itemId) {
//            R.id.m_setting -> (activity as MainAct).showSettingPage()
//        }
//
//        return true
//    }

    private fun initView() {
//        initToolbar(toolbar)
        tv_no_data.setText(R.string.message_empty_events)

        adapter = EventAdapter(list, object : OnItemClickListener {
            override fun onItemClicked(position: Int) {
                showKejadian(position)
            }
        })
        rv_main.layoutManager = LinearLayoutManager(App.context, LinearLayoutManager.VERTICAL, false)
        rv_main.adapter = adapter
        adapter.addSeparator(rv_main, R.dimen.card_space)

    }

    private fun showAddPointPage() {
        launchActivity(PointAct::class.java)
        activity?.finish()
    }

    private lateinit var data: Kejadian

    private fun showKejadian(position: Int) {
        val data = list[position]
        val intent = Intent(context, EventAct::class.java)
        intent.putExtra("id", data.id)
        startActivity(intent)
    }

    override fun showLoading(context: Context) {
        if (vf_main == null) return
        vf_main.displayedChild = if (list.isEmpty()) 1 else 0
    }

    override fun hideLoading() {
        if (vf_main == null) return
        vf_main.displayedChild = if (list.isEmpty()) 2 else 0
    }
}
