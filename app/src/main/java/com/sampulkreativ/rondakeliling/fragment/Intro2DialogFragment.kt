package com.sampulkreativ.rondakeliling.fragment

import android.os.Bundle
import com.rri.customer.fragment.core.CoreDialogFragment
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.listener.OnClickListener
import kotlinx.android.synthetic.main.dialog_intro.*


/**
 * Fragment for content in
 * Created by MuhammadLucky on 1/25/2017.
 */
class Intro2DialogFragment : CoreDialogFragment() {
    override val viewRes: Int? = R.layout.dialog_intro_2

    var text: String? = null
    internal var listener: OnClickListener? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        b_proceed.setOnClickListener { listener?.onClicked() }
    }
}
