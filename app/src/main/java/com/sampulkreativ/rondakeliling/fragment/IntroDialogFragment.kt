package com.sampulkreativ.rondakeliling.fragment

import android.os.Bundle
import com.rri.customer.fragment.core.CoreDialogFragment
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.listener.OnClickListener
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import kotlinx.android.synthetic.main.dialog_intro.*


/**
 * Fragment for content in
 * Created by MuhammadLucky on 1/25/2017.
 */
class IntroDialogFragment : CoreDialogFragment() {
    override val viewRes: Int? = R.layout.dialog_intro

    var text: String? = null
    internal var listener: OnClickListener? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tv_code.text = "Kode Pos Kamling: " + MemberPresenter().getPasswordFromPos()
        b_proceed.setOnClickListener { listener?.onClicked() }
    }
}
