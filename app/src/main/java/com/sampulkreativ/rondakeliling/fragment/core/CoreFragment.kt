package com.rri.customer.fragment.core

import android.content.Context
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rri.customer.activity.core.CoreActivity
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.helper.IntentHelper

/**
 * Created by Alvin Rusli on 1/24/2016.
 *
 * The parent fragment, all other fragments **should** extend from this class.
 */
abstract class CoreFragment : Fragment() {

    /**
     * The layout res for the current fragment.
     * @return the layout resource ID, return a null to make the fragment not set a content view.
     */
    abstract val viewRes: Int?

    @CallSuper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        // Initialize the fragment's view binding
        val view: View?
        if (viewRes != null) {
            view = inflater.inflate(viewRes!!, container, false)
        } else {
            view = null
        }

        return view
    }

    open fun initToolbar(toolbar: Toolbar) {
        (activity as CoreActivity).initToolbar(toolbar, true)
    }

    /** Show wait progress dialog with general message */
    open fun showLoading(context: Context) {
        Common.showProgressDialog(context, R.string.message_wait, null)
    }

    /** Hide wait progress dialog with */
    open fun hideLoading() {
        Common.dismissProgressDialog()
    }

    internal fun launchActivity(java: Class<*>) {
        if (context == null) return
        IntentHelper.launch(context!!, java)
    }
}
