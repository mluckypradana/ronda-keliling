package com.rri.customer.fragment.core

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText

/**
 * Created by Alvin Rusli on 1/24/2016.
 *
 * The parent fragment, all other fragments **should** extend from this class.
 */
abstract class CoreDialogFragment : DialogFragment() {

    /**
     * The layout res for the current fragment.
     * @return the layout resource ID, return a null to make the fragment not set a content view.
     */
    abstract val viewRes: Int?

    @CallSuper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
        // Initialize the fragment's view binding
        val view: View? = if (viewRes != null) {
            inflater.inflate(viewRes!!, container, false)
        } else {
            null
        }

        return view
    }

    protected fun EditText.body(): String = text.toString().trim()
}
