package com.sampulkreativ.rondakeliling.fragment

import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.api.callback.PointEvent
import com.rri.customer.api.callback.QrCodeEvent
import com.rri.customer.api.callback.RondaEvent
import com.rri.customer.fragment.core.CoreFragment
import com.rri.customer.helper.DateHelper
import com.rri.customer.listener.OnItemClickListener
import com.sampulkreativ.rondakeliling.BuildConfig
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.activity.PointAct
import com.sampulkreativ.rondakeliling.activity.SimpleScannerActivity
import com.sampulkreativ.rondakeliling.adapter.MemberAdapter
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.config.Config
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.helper.ResourceHelper
import com.sampulkreativ.rondakeliling.model.*
import com.sampulkreativ.rondakeliling.model.temp.MemberMarker
import com.sampulkreativ.rondakeliling.presenter.LocationPresenter
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import com.sampulkreativ.rondakeliling.presenter.PointPresenter
import com.sampulkreativ.rondakeliling.presenter.RondaPresenter
import kotlinx.android.synthetic.main.fragment_ronda.*
import kotlinx.android.synthetic.main.layout_no_data.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


/**
 * Fragment for content in
 * Created by MuhammadLucky on 1/25/2017.
 */
class MainFragment : CoreFragment(), OnMapReadyCallback {
    private var memberPresenter = MemberPresenter()
    private var pointPresenter = PointPresenter()
    private var presenter = RondaPresenter()
    private val list: MutableList<Member> = arrayListOf()
    private val pointList: MutableList<TitikRonda> = arrayListOf()
    private val pointMarkerList: MutableList<Marker> = arrayListOf()
    private val memberMarkers: MutableList<MemberMarker> = arrayListOf()
    private lateinit var adapter: MemberAdapter
    private lateinit var map: GoogleMap

    override val viewRes: Int? = R.layout.fragment_ronda

    companion object {
        var ACTIVE = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        initView()
        initMap()
        initData()

    }

    private lateinit var pos: Pos

    private var selectedMarker: Marker? = null
    private var selectedPoint: TitikRonda? = null

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap!!
        map.uiSettings.isMapToolbarEnabled = false
        map.isMyLocationEnabled = true
        map.getMyLocation()

        //Zoom to pos
        pos = memberPresenter.getMemberPlace()!!
        addPosMarker()

        val defaultLocation = LatLng(pos.latitude!!, pos.longitude!!)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLocation, Config.DEFAULT_ZOOM))

        setupMapListener()

        setMapStyleNoText()
        initMembers()


        presenter.isRondaStarted(null)
    }

    private fun setupMapListener() {
        map.setOnMarkerClickListener {
            selectedMarker = it
            selectedPoint = pointList.find {
                it.latitude == selectedMarker?.position?.latitude
                        && it.longitude == selectedMarker?.position?.longitude
            }
            false
        }
        val myLocationChangeListener = GoogleMap.OnMyLocationChangeListener { location ->
            val loc = LatLng(location.latitude, location.longitude)
            MemberPresenter.updateMemberLocation(loc)
        }
        map.setOnMyLocationChangeListener(myLocationChangeListener)
    }

    private fun initMembers() {
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.app_name)
        memberPresenter.getMembers(object : FetchDataCallback {
            override fun onFailure(message: String?) {
            }

            override fun onSuccess(data: Any?) {
                list.clear()
                list.addAll(data as MutableList<Member>)
                member = list.find { it.id == member?.id }
//                if (ronda != null)
                addMemberMarkers()
            }
        })
    }

    private fun initPoint() {
        pointPresenter.getPoints(false, object : FetchDataCallback {
            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(data: Any?) {
                var dataList = data as MutableList<TitikRonda>
                pointPresenter.getPointCheckpoints(ronda, dataList, object : FetchDataCallback {
                    override fun onFailure(message: String?) {
                        Common.showToast(message)
                    }

                    override fun onSuccess(data: Any?) {
                        addDotMarkers(dataList)
                    }
                })
            }
        })


    }

    private fun updateDotMarkers(dataList: MutableList<TitikRonda>) {
        for ((i, data) in pointList.withIndex())
            updateMarkerTitle(data, pointMarkerList[i])

    }

    private fun addDotMarkers(list: MutableList<TitikRonda>) {
        //Remove all marker
        for (marker in pointMarkerList)
            marker.remove()

        pointList.clear()
        pointList.addAll(list)

        //Populate to list
        pointMarkerList.clear()
        for (point in list) {
            val location = Location("serviceprovider")
            location.latitude = point.latitude!!
            location.longitude = point.longitude!!

            // Creating a marker
            val markerOptions = MarkerOptions().title(getString(R.string.label_point))
                    .icon(BitmapDescriptorFactory.fromBitmap(ResourceHelper.getBitmap(context!!, R.drawable.ic_dot)))
            val latlng = LatLng(location.latitude, location.longitude)
            markerOptions.position(latlng)
            markerOptions.zIndex(20.0f)

            var marker = map.addMarker(markerOptions)
            updateMarkerTitle(point, marker)

            pointMarkerList.add(marker)
        }


    }

    override fun onResume() {
        super.onResume()
        ACTIVE = true
        initToolbar(toolbar)
    }

    private fun addPosMarker() {
        // Creating a marker
        val markerOptions = MarkerOptions().icon(
                BitmapDescriptorFactory.fromBitmap(ResourceHelper.getBitmap(context!!, R.drawable.ic_home_primary)))
                .title(pos.name)
        val latlng = LatLng(pos.latitude!!, pos.longitude!!)
        markerOptions.position(latlng)
        markerOptions.zIndex(1.0f)
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, Config.DEFAULT_ZOOM))
        map.addMarker(markerOptions)
    }

    private fun initData() {
        member = MemberPresenter.getMember()
        if (!member!!.placeAdmin!!)
            fab_announce.visibility = View.GONE
    }

    private fun initView() {
        initToolbar(toolbar)
        setButtonAsStart()

        tv_no_data.setText(R.string.message_need_add_point)
        b_add.setOnClickListener { showAddPointPage() }
        tv_ronda.setOnClickListener { toggleRonda() }
        fab_event.setOnClickListener { showEvent() }
        fab_scan.setOnClickListener { showScanDialog() }
        fab_announce.setOnClickListener { showAnnounceDialog() }
    }

    private fun showAnnounceDialog() {
        var fragment = AnnounceDialogFragment()
        fragment.show(childFragmentManager, null)
    }

    private fun showScanDialog() {
        Common.showToast(R.string.message_scan_guide)
        launchActivity(SimpleScannerActivity::class.java)
    }

    private fun showEvent() {
        var fragment = EventDialogFragment()
        fragment.ronda = ronda
        fragment.listener = object : OnItemClickListener {
            override fun onItemClicked(position: Int) {
//                setPoint(fragment.text)
                fragment.dismiss()
            }
        }
        fragment.show(childFragmentManager, null)
    }

    private fun toggleRonda() {
        if (ronda == null) startRonda()
        else stopRonda()
    }

    private fun stopRonda() {
        AlertDialog.Builder(context!!)
                .setMessage(getString(R.string.message_confirm_stop_ronda))
                .setPositiveButton(R.string.action_yes) { dialog, which ->
                    proceedStopRonda()
                }.setNegativeButton(R.string.action_no) { dialog, which ->
                    dialog.dismiss()
                }.show()
    }

    private fun proceedStopRonda() {
        presenter.stopRonda(ronda, object : PostDataCallback {
            override fun onValidation(message: String) {
                Common.showToast(message)
            }

            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(data: Any?) {
                ronda = null
                setRondaStopped()
            }
        })
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onQRCodeScannedEvent(event: QrCodeEvent) {
        EventBus.getDefault().removeStickyEvent(event)
        addCheckpoint(event.text)
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onPointChangedEvent(event: PointEvent) {
        EventBus.getDefault().removeStickyEvent(event)
        initPoint()
    }

    @Subscribe//(sticky = true, threadMode = ThreadMode.MAIN)
    fun onRondaChanged(event: RondaEvent) {
//        EventBus.getDefault().removeE(event)
        ronda = event.ronda
        if (ronda != null)
            setRondaStarted()
        else
            setRondaStopped()

        initPoint()
    }

    private fun addCheckpoint(text: String?) {
        presenter.addCheckpoint(text, ronda, pointList, object : PostDataCallback {
            override fun onValidation(message: String) {
                Common.showToast(message)
            }

            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(data: Any?) {
                Common.showToast(R.string.success_add_checkpoint)
                updateMarkerTitle(data as Checkpoint)
            }
        })
    }

    private fun updateMarkerTitle(point: TitikRonda, marker: Marker) {
        if (point.checkpoints.isEmpty()) return
        marker.title = "Patroli terakhir: " + DateHelper.format(milis = point.checkpoints[point.checkpoints.size - 1].createdAt)
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(ResourceHelper.getBitmap(context!!,
                if (point.checkpoints.isEmpty()) R.drawable.ic_dot
                else R.drawable.ic_dot_green)))
    }

    private fun updateMarkerTitle(checkpoint: Checkpoint) {
        var point = pointList.find { it.id == checkpoint.pointId }
        point?.checkpoints?.add(checkpoint)

        var index = pointList.indexOf(point)
        var selectedPointMarker = pointMarkerList[index]
        updateMarkerTitle(point!!, selectedPointMarker)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onPause() {
        super.onPause()
        ACTIVE = false
        EventBus.getDefault().unregister(this)
    }

    private fun setButtonAsStart() {
//        b_start.setBackgroundColor(ContextCompat.getColor(context!!, R.color.primary))
        tv_ronda.setText(R.string.action_start_ronda)
        fab_event.visibility = View.VISIBLE
        fab_scan.visibility = View.GONE
    }

    private fun removeMemberMarkers() {
        for (data in memberMarkers)
            data.marker?.remove()
        memberMarkers.clear()
    }

    private fun showAddPointPage() {
        if (isVisible)
            launchActivity(PointAct::class.java)
    }

    private var ronda: Ronda? = null

    private var member: Member? = null

    private fun startRonda() {
        //Check gps
        var manager = App.context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (BuildConfig.FLAVOR != "demo" && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            LocationPresenter().showActivateGpsDialog(context!!, activity!!, null)
            Common.showToast(R.string.error_empty_location_ronda)
            return
        }

        //Start ronda
        presenter.startRonda(member?.latitude, member?.longitude, object : PostDataCallback {
            override fun onValidation(message: String) {
                Common.showToast(message)
            }

            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(data: Any?) {
                ronda = data as Ronda

                setRondaStarted()
            }
        })
    }

    private fun setRondaStarted() {
        if (ronda == null) return
//        val defaultLocation = LatLng(member?.latitude!!, member!!.longitude!!)
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLocation, 17F))

        setButtonAsStop()
        setMapStyle()
        addMemberMarkers()
    }

    private fun setRondaStopped() {
        setButtonAsStart()
        setMapStyleNoText()
    }

    private fun addMemberMarkers() {
        removeMemberMarkers()
        for (data in list) {
            if (data.latitude == null) continue
            if (data.id == member?.id) continue

            // Creating a marker
            val markerOptions = MarkerOptions().title(data.name).icon(
                    BitmapDescriptorFactory.fromBitmap(ResourceHelper.getBitmap(App.context, R.drawable.ic_person_pin_white)))
            val latlng = LatLng(data.latitude!!, data.longitude!!)
            markerOptions.position(latlng)
            markerOptions.zIndex(90.0f)
//            map.animateCamera(CameraUpdateFactory.newLatLng(latlng))

            var marker = map.addMarker(markerOptions)
            memberMarkers.add(MemberMarker(data, marker))
        }
    }

    private fun setButtonAsStop() {
//        b_start.setBackgroundColor(ContextCompat.getColor(context, R.color.button_red))
        if (tv_ronda == null) return
        tv_ronda.setText(R.string.action_stop_ronda)
        fab_event.visibility = View.VISIBLE
        fab_scan.visibility = View.VISIBLE
    }

    private fun initMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = childFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun setMapStyle() {
        val style = MapStyleOptions.loadRawResourceStyle(context, R.raw.map_style)
        map.setMapStyle(style)
    }

    private fun setMapStyleNoText() {
        val style = MapStyleOptions.loadRawResourceStyle(context, R.raw.map_style_no_text)
        map.setMapStyle(style)
    }

    private lateinit var data: Member

    override fun showLoading(context: Context) {
        if (vf_main == null) return
        vf_main.displayedChild = if (list.isEmpty()) 1 else 0
    }

    override fun hideLoading() {
        if (vf_main == null) return
        vf_main.displayedChild = if (list.isEmpty()) 2 else 0
    }
}
