package com.sampulkreativ.rondakeliling.fragment

import android.os.Bundle
import android.widget.Toast
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.fragment.core.CoreDialogFragment
import com.rri.customer.listener.OnItemClickListener
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.model.Kejadian
import com.sampulkreativ.rondakeliling.model.Ronda
import com.sampulkreativ.rondakeliling.presenter.EventPresenter
import kotlinx.android.synthetic.main.dialog_event.*


/**
 * Fragment for content in
 * Created by MuhammadLucky on 1/25/2017.
 */
class EventDialogFragment : CoreDialogFragment() {
    override val viewRes: Int? = R.layout.dialog_event
    val presenter = EventPresenter()

    var text: String? = null
    internal var listener: OnItemClickListener? = null
    internal var ronda: Ronda? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        b_theft.setOnClickListener { report(Kejadian.THEFT) }
        b_fire.setOnClickListener { report(Kejadian.FIRE) }
        b_accident.setOnClickListener { report(Kejadian.ACCIDENT) }
        b_disaster.setOnClickListener { report(Kejadian.DISASTER) }
        b_proceed.setOnClickListener { report(Kejadian.OTHER) }
    }

    private fun report(code: Int) {
        presenter.report(code, ronda, et_other.body(), object : PostDataCallback {
            override fun onValidation(message: String) {
                Common.showToast(message)
            }

            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(data: Any?) {
                Toast.makeText(App.context, data as String, Toast.LENGTH_LONG).show()
                dismiss()
            }
        })
    }
}
