package com.sampulkreativ.rondakeliling.fragment

import android.os.Bundle
import android.util.SparseArray
import com.budiyev.android.codescanner.CodeScanner
import com.google.android.gms.vision.barcode.Barcode
import com.rri.customer.fragment.core.CoreDialogFragment
import com.rri.customer.listener.OnItemClickListener
import com.sampulkreativ.rondakeliling.R
import info.androidhive.barcode.BarcodeReader


/**
 * Fragment for content in
 * Created by MuhammadLucky on 1/25/2017.
 */
class AddQrCodeDialogV2Fragment : CoreDialogFragment() , BarcodeReader.BarcodeReaderListener{
    override fun onBitmapScanned(sparseArray: SparseArray<Barcode>?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onScannedMultiple(barcodes: MutableList<Barcode>?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCameraPermissionDenied() {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onScanned(barcode: Barcode?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        dismiss()
    }

    override fun onScanError(errorMessage: String?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private lateinit var codeScanner: CodeScanner
    override val viewRes: Int? = R.layout.dialog_add_qrcode_ii

    var text: String? = null
    internal var listener: OnItemClickListener? = null

    private lateinit var barcodeReader: BarcodeReader

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        barcodeReader = childFragmentManager.findFragmentById(R.id.barcode_fragment) as BarcodeReader
    }
}
