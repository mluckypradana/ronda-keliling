package com.sampulkreativ.rondakeliling.fragment

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.applandeo.materialcalendarview.EventDay
import com.applandeo.materialcalendarview.listeners.OnDayClickListener
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.rri.customer.fragment.core.CoreFragment
import com.rri.customer.listener.OnItemClickListener
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.activity.PointAct
import com.sampulkreativ.rondakeliling.activity.RondaAct
import com.sampulkreativ.rondakeliling.adapter.RondaAdapter
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.model.Ronda
import com.sampulkreativ.rondakeliling.presenter.PointPresenter
import com.sampulkreativ.rondakeliling.presenter.PrefPresenter
import com.sampulkreativ.rondakeliling.presenter.RondaPresenter
import kotlinx.android.synthetic.main.fragment_history.*
import kotlinx.android.synthetic.main.fragment_ronda_list.*
import kotlinx.android.synthetic.main.layout_no_data.*
import java.util.*


/**
 * Fragment for content in
 * Created by MuhammadLucky on 1/25/2017.
 */
class RondasFragment : CoreFragment() {

    private var presenter = RondaPresenter()
    private var pointPresenter = PointPresenter()
    private val list: MutableList<Ronda> = arrayListOf()
    private val filteredList: MutableList<Ronda> = arrayListOf()
    private lateinit var adapter: RondaAdapter
    private lateinit var filteredAdapter: RondaAdapter

    override val viewRes: Int? = R.layout.fragment_ronda_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        initView()
        initData()
        initCalendar()
    }

    private fun initCalendar() {
        cv_main.setOnDayClickListener(OnDayClickListener { eventDay ->
            val calendar = eventDay.calendar
            val endCalendar = Calendar.getInstance()
            endCalendar.timeInMillis = calendar.time.time
            endCalendar.add(Calendar.DAY_OF_MONTH, 1)
            var filtered = list.filter {
                it.endAt != null && it.endAt!! >= calendar.time.time && it.endAt!! <= endCalendar.time.time
            }
            filteredList.clear()
            filteredList.addAll(filtered)
            filteredAdapter.notifyDataSetChanged()
        })
    }

    private fun setupCalendar() {
        var events = arrayListOf<EventDay>()

        for (data in list) {
            if (data.endAt == null) continue
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = data.endAt!!
            events.add(EventDay(calendar, R.drawable.ic_directions_walk))

        }

        if (cv_main != null)
            cv_main.setEvents(events)
    }

    private var pointTotal: Int = 0

    private fun initData() {

        showLoading(context!!)
        presenter.getRondaList(true, object : FetchDataCallback {
            override fun onFailure(message: String?) {
                Common.showToast(message)
                hideLoading()
            }

            override fun onSuccess(data: Any?) {
                refreshData(data as MutableList<Ronda>)
                hideLoading()
            }
        })
    }

    private fun refreshData(list: MutableList<Ronda>) {
        this.list.clear()
        this.list.addAll(list)
        adapter.notifyDataSetChanged()

        setupCalendar()
    }

    override fun onResume() {
        super.onResume()
        if (toolbar != null) {
            initToolbar(toolbar)
        }
    }

    private fun initView() {
//        initToolbar(toolbar)
        tv_no_data.setText(R.string.message_empty_ronda)
        b_list.setOnClickListener { switchView(0) }
        b_date.setOnClickListener { switchView(1) }

        adapter = RondaAdapter(list, object : OnItemClickListener {
            override fun onItemClicked(position: Int) {
                showRonda(position)
            }
        })
        rv_main.layoutManager = LinearLayoutManager(App.context, LinearLayoutManager.VERTICAL, false)
        rv_main.adapter = adapter
        adapter.addSeparator(rv_main, R.dimen.card_space)

        filteredAdapter = RondaAdapter(filteredList, object : OnItemClickListener {
            override fun onItemClicked(position: Int) {
                showRonda(position)
            }
        })
        rv_filtered.isNestedScrollingEnabled = false
        rv_filtered.layoutManager = LinearLayoutManager(App.context, LinearLayoutManager.VERTICAL, false)
        rv_filtered.adapter = filteredAdapter
        filteredAdapter.addSeparator(rv_filtered, R.dimen.card_space)

    }

    private fun switchView(type: Int) {
        vf_view.displayedChild = type
    }

    private fun showAddPointPage() {
        launchActivity(PointAct::class.java)
        activity?.finish()
    }

    private lateinit var data: Ronda

    private fun showRonda(position: Int) {
        val data = list[position]
        PrefPresenter.setData(data)
        launchActivity(RondaAct::class.java)
    }

    override fun showLoading(context: Context) {
        if (vf_main == null) return
        vf_main.displayedChild = if (list.isEmpty()) 1 else 0
    }

    override fun hideLoading() {
        if (vf_main == null) return
        vf_main.displayedChild = if (list.isEmpty()) 2 else 0
    }
}
