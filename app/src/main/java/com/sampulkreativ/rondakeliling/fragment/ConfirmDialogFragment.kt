package com.sampulkreativ.rondakeliling.fragment

import android.os.Bundle
import android.widget.LinearLayout
import com.rri.customer.fragment.core.CoreDialogFragment
import com.sampulkreativ.rondakeliling.listener.OnClickListener
import com.sampulkreativ.rondakeliling.R
import kotlinx.android.synthetic.main.dia_confirm.*


/**
 * Fragment for content in
 * Created by MuhammadLucky on 1/25/2017.
 */
class ConfirmDialogFragment : CoreDialogFragment() {
    override val viewRes: Int? = R.layout.dia_confirm
    private var headerIconResId: Int = R.mipmap.ic_launcher
    private var message: String? = null
    private var listener: OnClickListener? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tv_description.text = message
        iv_main.setImageResource(headerIconResId)
        b_ok.setOnClickListener {
            dismiss()
            listener?.onClicked()
        }
    }

    override fun onResume() {
        super.onResume()
        val params = dialog.window!!.attributes
        params.width = LinearLayout.LayoutParams.MATCH_PARENT
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT
        params.horizontalMargin = 64f
        dialog.window!!.attributes = params as android.view.WindowManager.LayoutParams
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun setListener(listener: OnClickListener?) {
        this.listener = listener
    }

    fun setHeaderIcon(headerIconResId: Int) {
        this.headerIconResId = headerIconResId
    }
}
