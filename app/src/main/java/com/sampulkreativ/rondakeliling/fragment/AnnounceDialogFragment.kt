package com.sampulkreativ.rondakeliling.fragment

import android.os.Bundle
import com.kopnus.kdigi.presenter.callback.PostDataCallback
import com.rri.customer.fragment.core.CoreDialogFragment
import com.rri.customer.listener.OnItemClickListener
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.presenter.AnnouncePresenter
import kotlinx.android.synthetic.main.dialog_announce.*


/**
 * Fragment for content in
 * Created by MuhammadLucky on 1/25/2017.
 */
class AnnounceDialogFragment : CoreDialogFragment() {
    override val viewRes: Int? = R.layout.dialog_announce
    val presenter = AnnouncePresenter()

    var text: String? = null
    internal var listener: OnItemClickListener? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        b_proceed.setOnClickListener { proceed() }
    }

    private fun proceed() {
        presenter.announce(et_title.body(), et_description.body(), object : PostDataCallback {
            override fun onValidation(message: String) {
                Common.showToast(message)
            }

            override fun onFailure(message: String?) {
                Common.showToast(message)
            }

            override fun onSuccess(data: Any?) {
                Common.showToast(data as String)
                dismiss()
            }
        })
    }
}
