package com.sampulkreativ.rondakeliling.fragment

import android.os.Bundle
import android.view.View
import com.rri.customer.fragment.core.CoreFragment
import com.sampulkreativ.Kejadiankeliling.fragment.AnnouncementsFragment
import com.sampulkreativ.Kejadiankeliling.fragment.EventsFragment
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.adapter.HistoryPagerAdapter
import com.sampulkreativ.rondakeliling.model.Ronda
import com.sampulkreativ.rondakeliling.presenter.PointPresenter
import com.sampulkreativ.rondakeliling.presenter.RondaPresenter
import kotlinx.android.synthetic.main.fragment_history.*

/**
 * Fragment for content in
 * Created by MuhammadLucky on 1/25/2017.
 */
class HistoryFragment : CoreFragment() {

    private var presenter = RondaPresenter()
    private var pointPresenter = PointPresenter()
    private val list: MutableList<Ronda> = arrayListOf()
    private lateinit var adapter: HistoryPagerAdapter

    override val viewRes: Int? = R.layout.fragment_history

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

//        initToolbar(toolbar)
        initTab()
    }

    private fun initTab() {
        var rondasFragment = RondasFragment()
        var eventsFragment = EventsFragment()
        var announcementsFragment = AnnouncementsFragment()

        adapter = HistoryPagerAdapter(childFragmentManager)
        adapter.addFragment(rondasFragment, getString(R.string.title_ronda))
        adapter.addFragment(eventsFragment, getString(R.string.title_events))
        adapter.addFragment(announcementsFragment, getString(R.string.title_announcements))
        vp_main.adapter = adapter
        tl_main.setupWithViewPager(vp_main)
    }

}
