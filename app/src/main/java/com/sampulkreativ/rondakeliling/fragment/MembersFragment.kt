package com.sampulkreativ.rondakeliling.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.rri.customer.fragment.core.CoreFragment
import com.rri.customer.listener.OnItemClickListener
import com.sampulkreativ.rondakeliling.R
import com.sampulkreativ.rondakeliling.activity.AddMemberAct
import com.sampulkreativ.rondakeliling.activity.PointAct
import com.sampulkreativ.rondakeliling.adapter.MemberAdapter
import com.sampulkreativ.rondakeliling.app.App
import com.sampulkreativ.rondakeliling.helper.Common
import com.sampulkreativ.rondakeliling.model.Member
import com.sampulkreativ.rondakeliling.model.TitikRonda
import com.sampulkreativ.rondakeliling.presenter.MemberPresenter
import com.sampulkreativ.rondakeliling.presenter.PointPresenter
import kotlinx.android.synthetic.main.fragment_members.*
import kotlinx.android.synthetic.main.layout_no_data.*
import kotlinx.android.synthetic.main.layout_toolbar.*


/**
 * Fragment for content in
 * Created by MuhammadLucky on 1/25/2017.
 */
class MembersFragment : CoreFragment() {

    private var presenter = MemberPresenter()
    private var pointPresenter = PointPresenter()
    private val list: MutableList<Member> = arrayListOf()
    private lateinit var adapter: MemberAdapter

    override val viewRes: Int? = R.layout.fragment_members

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        initView()
        initData()
    }

    private var pointTotal: Int = 0

    private lateinit var member: Member

    private fun initData() {
        member = MemberPresenter.getMember()!!
        toolbar.title = getString(R.string.title_membership)

        pointPresenter.getPoints(false, object : FetchDataCallback {
            override fun onFailure(message: String?) {
            }

            override fun onSuccess(data: Any?) {
                val list = data as MutableList<TitikRonda>
                pointTotal = list.size
                updateNextVisibility()
            }
        })

        showLoading(context!!)
        presenter.getMembers(object : FetchDataCallback {
            override fun onFailure(message: String?) {
                Common.showToast(message)
                hideLoading()
            }

            override fun onSuccess(data: Any?) {
                refreshData(data as MutableList<Member>)
                hideLoading()
                updateNextVisibility()
            }
        })
    }

    private fun updateNextVisibility() {
//        b_next.visibility = if (pointTotal == 0 && list.size > 0) View.VISIBLE else View.GONE
    }

    private fun refreshData(list: MutableList<Member>) {
        if (member.placeAdmin) {
            var admin = list.find { it.id == member.id }
            list.remove(admin)
        }

        this.list.clear()
        this.list.addAll(list)
        adapter.notifyDataSetChanged()
    }

//    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
//        // Do something that differs the Activity's menu here
//        inflater?.inflate(R.menu.setting, menu);
//        super.onCreateOptionsMenu(menu, inflater)
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        when (item!!.itemId) {
//            R.id.m_setting -> (activity as MainAct).showSettingPage()
//        }
//
//        return true
//    }

    private fun initView() {
        initToolbar(toolbar)
        tv_code.text = "Kode Pos Kamling: " + MemberPresenter().getPasswordFromPos()
        tv_code.setOnClickListener { showCodeInfo() }
        tv_no_data.setText(R.string.message_need_add_member)
        b_next.setOnClickListener { showAddPointPage() }
        b_add.setOnClickListener { showAddMemberPage() }

        adapter = MemberAdapter(list, object : OnItemClickListener {
            override fun onItemClicked(position: Int) {
                showMember(position)
            }
        })
        rv_main.layoutManager = LinearLayoutManager(App.context, LinearLayoutManager.VERTICAL, false)
        rv_main.adapter = adapter
        adapter.addSeparator(rv_main, R.dimen.card_space)

    }

    private fun showAddMemberPage() {
        launchActivity(AddMemberAct::class.java)
    }

    private fun showCodeInfo() {
        Common.showMessageDialog(context = context!!, message = getString(R.string.message_need_add_member))
    }

    private fun showAddPointPage() {
        launchActivity(PointAct::class.java)
        activity?.finish()
    }

    private lateinit var data: Member

    private fun showMember(position: Int) {
        val data = list[position]

        Dexter.withActivity(activity)
                .withPermission(Manifest.permission.CALL_PHONE)
                .withListener(object : PermissionListener {
                    @SuppressLint("MissingPermission")
                    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + data.phone))
                        startActivity(intent)
                    }

                    override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                        Common.showToast(R.string.message_permission_call)
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                        Common.showToast(R.string.error_permission_denied_call)
                    }
                }).check()
    }

    override fun showLoading(context: Context) {
        if (vf_main == null) return
        vf_main.displayedChild = if (list.isEmpty()) 1 else 0
    }

    override fun hideLoading() {
        if (vf_main == null) return
        vf_main.displayedChild = if (list.isEmpty()) 2 else 0
    }
}
