package com.sampulkreativ.rondakeliling.fragment

import android.os.Bundle
import android.widget.Toast
import com.budiyev.android.codescanner.*
import com.rri.customer.fragment.core.CoreDialogFragment
import com.rri.customer.listener.OnItemClickListener
import com.sampulkreativ.rondakeliling.R
import kotlinx.android.synthetic.main.dialog_add_qrcode.*


/**
 * Fragment for content in
 * Created by MuhammadLucky on 1/25/2017.
 */
class AddQrCodeDialogFragment : CoreDialogFragment() {
    private lateinit var codeScanner: CodeScanner
    override val viewRes: Int? = R.layout.dialog_add_qrcode

    var text: String? = null
    internal var listener: OnItemClickListener? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        codeScanner = CodeScanner(context!!, scanner_view)

        // Parameters (default values)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ALL_FORMATS
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.CONTINUOUS // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        // Callbacks
        codeScanner.decodeCallback = DecodeCallback {
            activity?.runOnUiThread {
                text = it.text
                listener?.onItemClicked(0)
            }
        }
        codeScanner.errorCallback = ErrorCallback {
            // or ErrorCallback.SUPPRESS
            activity?.runOnUiThread {
                Toast.makeText(context!!, "Camera initialization error: ${it.message}",
                        Toast.LENGTH_LONG).show()
            }
        }
        scanner_view.setOnClickListener { codeScanner.startPreview() }
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }
}
